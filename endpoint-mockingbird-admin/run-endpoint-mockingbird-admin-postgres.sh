# Example Shell Script File to run endpoint-mockingbird-admin 
# Need to compile the code first using mvn clean install
# You will need to set up your postgres URL as POSTGRES_MOCKINGBIRD_URL or replace in the script below
# Note the full url is in the format jdbc:postgresql://POSTGRES_DB_URL:5432/postgres?user=POSTGRES_USERNAME&password=POSTGRES_PASSWORD
# The MOCKINGBIRD_SERVER value can be localhost or the IP\URL address of the server that is running the admin console.

nohup java -Dserver.port=9876 -Dserver.host=$MOCKINGBIRD_SERVER -Dspring.profiles.active=postgres \
-Dspring.jpa.database-platform=org.hibernate.dialect.PostgreSQL10Dialect  \
-Dspring.datasource.url=$POSTGRES_MOCKINGBIRD_URL \
-Dspring.application.host=$MOCKINGBIRD_SERVER \
-jar ./target/endpoint-mockingbird-admin-spring-boot.war > ./output.log 2>&1 &
echo $! > pid.file