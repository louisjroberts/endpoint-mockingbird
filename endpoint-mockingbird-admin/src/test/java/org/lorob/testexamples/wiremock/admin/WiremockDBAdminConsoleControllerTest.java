package org.lorob.testexamples.wiremock.admin;

import static org.junit.Assert.assertEquals;
import javax.annotation.PostConstruct;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import io.restassured.path.xml.XmlPath;
import io.restassured.path.xml.XmlPath.CompatibilityMode;
import io.restassured.response.Response;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class WiremockDBAdminConsoleControllerTest {

	@LocalServerPort
	private int port;

	private String baseUri;

	private String uri;

	@PostConstruct
	public void init() {
		baseUri = "http://localhost:" + port;
		uri = baseUri;
	}

	@AfterAll
	public static void teardown() {
		RestAssuredMockMvc.reset();
	}

	@Test
	public void testDefaultPage() {
		final Response response = RestAssured.given().get(uri).then().contentType(ContentType.HTML).extract()
				.response();
		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
		XmlPath htmlPath = new XmlPath(CompatibilityMode.HTML, response.getBody().asString());
		assertEquals("Home Page", htmlPath.getString("html.head.title"));
	}

	@Test
	public void anonymousUsersShouldGetNotAuthenticated() throws Exception {
		final Response response = RestAssured.given().get(uri).then().contentType(ContentType.HTML).extract()
				.response();
		XmlPath htmlPath = new XmlPath(CompatibilityMode.HTML, response.getBody().asString());
		assertEquals("Home Page", htmlPath.getString("html.head.title"));
		assertEquals("Log in with Amazon Cognito", htmlPath.getString("html.body.div[0]").trim());
	}

	@Test
	public void testHomePage() {
		final Response response = RestAssured.given().get(uri + "/home").then().contentType(ContentType.HTML).extract()
				.response();
		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
		XmlPath htmlPath = new XmlPath(CompatibilityMode.HTML, response.getBody().asString());
		assertEquals("Home Page", htmlPath.getString("html.head.title"));
	}
	
	@Test
	public void testAccessDenied() {
		final Response response = RestAssured.given().get(uri + "/accessDenied").then().contentType(ContentType.HTML).extract()
				.response();
		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
		XmlPath htmlPath = new XmlPath(CompatibilityMode.HTML, response.getBody().asString());
		assertEquals("Access Denied Page", htmlPath.getString("html.head.title"));
	}
	
	@Test
	public void testlogoutSuccess() {
		final Response response = RestAssured.given().get(uri + "/logoutsuccess").then().contentType(ContentType.HTML).extract()
				.response();
		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
		XmlPath htmlPath = new XmlPath(CompatibilityMode.HTML, response.getBody().asString());
		assertEquals("Logout Page", htmlPath.getString("html.head.title"));
	}

	@Test
	public void testStartAllMocks() {
		final Response response = RestAssured.given().get(uri + "/startall").then().contentType(ContentType.HTML)
				.extract().response();
		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
		XmlPath htmlPath = new XmlPath(CompatibilityMode.HTML, response.getBody().asString());
		assertEquals("Endpoint Mockingbird - Display Mock Services Page", htmlPath.getString("html.head.title"));
		assertEquals("Test Endpoint MockingBird DB Admin Console - Show Mocks Page", htmlPath.getString("html.body.h1"));
	}

	@Test
	public void testShowMocks() {
		final Response response = RestAssured.given().get(uri + "/showmocks").then().contentType(ContentType.HTML)
				.extract().response();
		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
		XmlPath htmlPath = new XmlPath(CompatibilityMode.HTML, response.getBody().asString());
		assertEquals("Endpoint Mockingbird - Display Mock Services Page", htmlPath.getString("html.head.title"));
		assertEquals("Test Endpoint MockingBird DB Admin Console - Show Mocks Page", htmlPath.getString("html.body.h1"));
	}

	@Test
	public void testInsert() {
		Response response = insertMock();
		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
		XmlPath htmlPath = new XmlPath(CompatibilityMode.HTML, response.getBody().asString());
		assertEquals("Endpoint Mockingbird - Insert Mock Service Page", htmlPath.getString("html.head.title"));

		String id = htmlPath.getString("**.findAll { it.@name == 'wireMockServiceId' }[0]");
		deleteMock(id);
	}

	private Response deleteMock(String id) {
		final Response response = RestAssured.given().urlEncodingEnabled(true).param("id", id).get(uri + "/delete");
		return response;
	}

	private Response insertMock() {
		return insertDefaultGetMock(9999);
	}

	private Response insertDefaultGetMock(int mockPort) {
		final Response response = RestAssured.given().urlEncodingEnabled(true).param("host", "localhost")
				.param("port", Integer.toString(mockPort)).param("url", "/testservice").param("httpJsonRequest", "")
				.param("httpJsonRequestHeaders", "{\"Content-Type\":\"application/json; charset=UTF-8\"}")
				.param("httpJsonResponse", "OK").param("httpRequestType", "GET").param("httpResponseCode", "200")
				.param("httpResponseFileType", "application/json").param("runOrder", "100").get(uri + "/insert");
		return response;
	}
	
	private Response insertCustomMock(int mockPort, String httpRequestType, String httpResponseCode) {
		final Response response = RestAssured.given().urlEncodingEnabled(true).param("host", "localhost")
				.param("port", Integer.toString(mockPort)).param("url", "/testservice").param("httpJsonRequest", "")
				.param("httpJsonRequestHeaders", "{\"Content-Type\":\"application/json; charset=UTF-8\"}")
				.param("httpJsonResponse", "OK").param("httpRequestType", httpRequestType).param("httpResponseCode", httpResponseCode)
				.param("httpResponseFileType", "application/json").param("runOrder", "100").get(uri + "/insert");
		return response;
	}

	@Test
	public void testUpdate() {
		Response insertResponse = insertMock();
		assertEquals(HttpStatus.OK.value(), insertResponse.getStatusCode());

		XmlPath htmlPath = new XmlPath(CompatibilityMode.HTML, insertResponse.getBody().asString());
		String id = htmlPath.getString("**.findAll { it.@name == 'wireMockServiceId' }[0]");

		final Response response = RestAssured.given().urlEncodingEnabled(true).param("id", id)
				.param("host", "localhost").param("port", Integer.toString(9999)).param("url", "/testmock")
				.param("httpJsonRequest", "")
				.param("httpJsonRequestHeaders", "{\"Content-Type\":\"application/json; charset=UTF-8\"}")
				.param("httpJsonResponse", "OK").param("httpRequestType", "GET").param("httpResponseCode", "200")
				.param("httpResponseFileType", "application/json").param("runOrder", id).get(uri + "/update");
		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
		htmlPath = new XmlPath(CompatibilityMode.HTML, response.getBody().asString());
		assertEquals("Endpoint Mockingbird - Update Mock Service Page", htmlPath.getString("html.head.title"));

		deleteMock(id);
	}

	@Test
	public void testDelete() {
		Response insertResponse = insertMock();
		assertEquals(HttpStatus.OK.value(), insertResponse.getStatusCode());

		XmlPath htmlPath = new XmlPath(CompatibilityMode.HTML, insertResponse.getBody().asString());
		String id = htmlPath.getString("**.findAll { it.@name == 'wireMockServiceId' }[0]");

		final Response response = deleteMock(id);

		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
		htmlPath = new XmlPath(CompatibilityMode.HTML, response.getBody().asString());
		assertEquals("Endpoint Mockingbird - Delete Mock Service Page", htmlPath.getString("html.head.title"));
	}

	private void testShowTestDisplaysPageAndCanCallWitHResponse(Response insertResponse) {
		assertEquals(HttpStatus.OK.value(), insertResponse.getStatusCode());

		XmlPath htmlPath = new XmlPath(CompatibilityMode.HTML, insertResponse.getBody().asString());
		String id = htmlPath.getString("**.findAll { it.@name == 'wireMockServiceId' }[0]");

		final Response response = RestAssured.given().urlEncodingEnabled(true).param("id", id)
				.param("host", "localhost").get(uri + "/showtest");
		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
		htmlPath = new XmlPath(CompatibilityMode.HTML, response.getBody().asString());
		assertEquals("Endpoint Mockingbird - Test Mock Service Page", htmlPath.getString("html.head.title"));

		// test calling test
		final Response testResponse = RestAssured.given().urlEncodingEnabled(true).param("id", id)
				.param("host", "localhost").get(uri + "/testmock");
		assertEquals(HttpStatus.OK.value(), testResponse.getStatusCode());
		htmlPath = new XmlPath(CompatibilityMode.HTML, testResponse.getBody().asString());
		assertEquals("Endpoint Mockingbird - Test Mock Service Page", htmlPath.getString("html.head.title"));
		String status = htmlPath.getString("**.findAll { it.@name == 'testResult' }[0]");
		assertEquals("200", status);

		deleteMock(id);
	}
	
	@Test
	public void testShowTestDisplaysPageAndCanCallTestMockOnGet() {
		Response insertResponse = insertCustomMock(9999,"GET","200");
		testShowTestDisplaysPageAndCanCallWitHResponse(insertResponse);
	}
	
	@Test
	public void testShowTestDisplaysPageAndCanCallTestMockOnPost() {
		Response insertResponse = insertCustomMock(9999,"POST","200");
		testShowTestDisplaysPageAndCanCallWitHResponse(insertResponse);
	}
	
	@Test
	public void testShowTestDisplaysPageAndCanCallTestMockOnPatch() {
		Response insertResponse = insertCustomMock(9999,"PATCH","200");
		testShowTestDisplaysPageAndCanCallWitHResponse(insertResponse);
	}
	
	@Test
	public void testShowTestDisplaysPageAndCanCallTestMockOnDelete() {
		Response insertResponse = insertCustomMock(9999,"DELETE","200");
		testShowTestDisplaysPageAndCanCallWitHResponse(insertResponse);
	}

}
