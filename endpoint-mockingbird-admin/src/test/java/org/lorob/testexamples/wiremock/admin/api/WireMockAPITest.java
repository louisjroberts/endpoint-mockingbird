package org.lorob.testexamples.wiremock.admin.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.lorob.testexamples.wiremock.admin.persistence.model.WireMockService;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import io.restassured.RestAssured;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import io.restassured.response.Response;

// Note these tests are destructive of the database being used
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class WireMockAPITest {
	
	private String API_ROOT = "/api/wiremockservice";
	
	@LocalServerPort
    private int port;

    private String baseUri;
    
    private String uri;
    
    @PostConstruct
    public void init() {
    	baseUri = "http://localhost:" + port;
        uri = baseUri + API_ROOT;
    }
    
    @AfterAll
    public static void teardown() {
    	RestAssuredMockMvc.reset();
    }
    
    @Test
    public void testWhenGetAllWireMockService_thenOK() {
    	final Response response = RestAssured.given()
                .get(uri);
    	assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }
    
    @Test
    public void testWhenGetCreatedWireMockServiceById_thenOK() {
        final WireMockService wireMockService = createRandomWireMockService();
        final String location = createWireMockServiceAsUri(wireMockService);

        final Response response = RestAssured.get(location);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertTrue(wireMockService.getPort() == (Integer)(response.jsonPath().get("port")));
        
        // tidy up
        RestAssured.delete(location);
    }
    
    @Test
    public void testWhenGetNotExistWireMockServiceById_thenNotFound() {
        final Response response = RestAssured.get(uri + "/-1");
        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
    }
    
    // POST
    @Test
    public void testWhenCreateNewWireMockService_thenCreated() {
        final WireMockService wireMockService = createRandomWireMockService();

        final Response response = RestAssured.given()
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(wireMockService)
            .post(uri);
        assertEquals(HttpStatus.CREATED.value(), response.getStatusCode());
        
        // tidy up
        RestAssured.delete(uri + "/" + response.jsonPath().get("id"));
    }
    
    @Test
    public void testWhenInvalidWireMockService_thenError() {
        final WireMockService wireMockService = createRandomWireMockService();
        wireMockService.setUrl(null);
        // NOTE - Spring is a bit flaky here. The object does not allow nullable values
        // which throw throws a validation exception that is caught by the @ControllerAdvice
        // class. HOWEVER, if validating the port which should be a positive value then
        // the exception is thrown at a different level and wrapped so that 
        // the @ControllerAdvice class does not intercept it.
        // So can validate nullable but not not min(0) !

        final Response response = RestAssured.given()
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(wireMockService)
            .post(uri);
        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode());
    }

    @Test
    public void testWhenUpdateCreatedWireMockService_thenUpdated() {
        final WireMockService wireMockService = createRandomWireMockService();
        final String location = createWireMockServiceAsUri(wireMockService);

        wireMockService.setId(Long.parseLong(location.split("api/wiremockservice/")[1]));
        wireMockService.setPort(9998);
        Response response = RestAssured.given()
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(wireMockService)
            .put(location);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());

        response = RestAssured.get(location);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertTrue(9998 == (Integer)(response.jsonPath().get("port")));
        
        // tidy up
        RestAssured.delete(location);
    }
    
    @Test
    public void testWhenUpdateWIthIncorrectID_thenCreatedMismatchExceptionThrownCausesBadRequest() {
    	final WireMockService wireMockService = createRandomWireMockService();
        final String location = createWireMockServiceAsUri(wireMockService);

        wireMockService.setId(Long.parseLong(location.split("api/wiremockservice/")[1]));
        wireMockService.setId(wireMockService.getId() + 1);
        wireMockService.setPort(9998);
       	Response response = RestAssured.given()
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(wireMockService)
            .put(location);
        	
        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode());
    }

    @Test
    public void testWhenDeleteCreatedWireMockService_thenOk() {
        final WireMockService WireMockService = createRandomWireMockService();
        final String location = createWireMockServiceAsUri(WireMockService);

        Response response = RestAssured.delete(location);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());

        response = RestAssured.get(location);
        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
    }
   
    
    /* Support functions */
    
    private WireMockService createRandomWireMockService() {
        WireMockService record = new WireMockService();
        record.setPort(9999);
        record.setUrl("/unittestingurl");
        record.setHttpJsonRequest("");
        record.setHttpJsonRequestHeaders("");
        record.setHttpJsonResponse("");
        record.setHttpRequestType("");
        record.setHttpResponseCode(200);
        record.setHttpResponseFileType("application/json");
        record.setRunOrder(100l);
        return record;
    }
 
    private String createWireMockServiceAsUri(WireMockService wireMockService) {
        Response response = RestAssured.given()
          .contentType(MediaType.APPLICATION_JSON_VALUE)
          .body(wireMockService)
          .post(uri);
        return uri + "/" + response.jsonPath().get("id");
    }
}
