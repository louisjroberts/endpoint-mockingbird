package org.lorob.testexamples.wiremock.admin.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.lorob.testexamples.wiremock.CustomWireMock;
import org.lorob.testexamples.wiremock.admin.persistence.model.WireMockService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;

@ExtendWith(MockitoExtension.class)
public class WireMockModelProcessorTest {

	@Mock
	WireMockAPI serviceRecordController;

	@InjectMocks
	WireMockModelProcessor wireMockModelProcessor = new WireMockModelProcessor() {
		public CustomWireMock getCustomWireMock() {
			return customWireMock;
		}
	};

	WireMockService expected;
	List<WireMockService> expectedList;
	MockHttpServletRequest httpRequest;

	@Mock
	CustomWireMock customWireMock;

	@BeforeEach
	public void setup() {
		expected = new WireMockService();
		expected.setId(1);
		expected.setPort(1111);
		expected.setHttpResponseCode(200);
		expected.setRunOrder(2l);

		httpRequest = new MockHttpServletRequest();
		httpRequest.setParameter("id", Long.toString(expected.getId()));
		httpRequest.setParameter("port", Integer.toString(expected.getPort()));
		httpRequest.setParameter("httpResponseCode", Integer.toString(expected.getHttpResponseCode()));
		httpRequest.setParameter("runOrder", Long.toString(expected.getRunOrder()));

		expectedList = new LinkedList<>();
		expectedList.add(expected);
		expectedList.add(expected);

		// customWireMock = Mockito.mock(CustomWireMock.class);
	}

	@Test
	public void testUpdate() throws Exception {
		Mockito.when(serviceRecordController.update(Mockito.any(WireMockService.class), Mockito.anyLong()))
				.thenReturn(expected);

		WireMockService actual = wireMockModelProcessor.update(httpRequest);
		assertEquals(expected, actual);
	}

	@Test
	public void testInsert() throws Exception {
		Mockito.when(serviceRecordController.create(Mockito.any(WireMockService.class))).thenReturn(expected);

		WireMockService actual = wireMockModelProcessor.insert(httpRequest);
		assertEquals(expected, actual);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void testFindAll() {
		Mockito.when(serviceRecordController.findAll()).thenReturn((Iterable) expectedList);
		Iterable<?> actual = wireMockModelProcessor.findAll();
		assertNotNull(actual);
	}

	@Test
	public void testDelete() throws Exception {
		wireMockModelProcessor.delete("1");
	}

	@Test
	public void testFindOne() throws Exception {
		Mockito.when(serviceRecordController.findOne(Mockito.anyLong())).thenReturn(expected);
		WireMockService actual = wireMockModelProcessor.findOne("1");
		assertEquals(expected, actual);
	}

	@Test
	public void testGetCustomMock() {
		CustomWireMock actualCustomWireMock = wireMockModelProcessor.getCustomWireMock();
		assertEquals(customWireMock, actualCustomWireMock);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void testStartAllMocks() {
		Mockito.when(serviceRecordController.findAllOrderByRunOrder()).thenReturn((Iterable) expectedList);
		Mockito.doNothing().when(customWireMock).processMockService(Mockito.any(WireMockService.class));
		wireMockModelProcessor.startAllMocks("localhost");
	}

}
