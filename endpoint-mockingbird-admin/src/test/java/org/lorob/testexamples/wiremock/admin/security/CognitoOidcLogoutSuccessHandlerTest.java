package org.lorob.testexamples.wiremock.admin.security;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

public class CognitoOidcLogoutSuccessHandlerTest {

	@Test
	public void testCognitoOidcLogoutSuccessHandler() {
		CognitoOidcLogoutSuccessHandler handler = new CognitoOidcLogoutSuccessHandler("logout","12345","redirectUrl");
		String actual = handler.determineTargetUrl(null, null, null);
		
		assertEquals("logout?client_id=12345&logout_uri=redirectUrl",actual);
	}
}
