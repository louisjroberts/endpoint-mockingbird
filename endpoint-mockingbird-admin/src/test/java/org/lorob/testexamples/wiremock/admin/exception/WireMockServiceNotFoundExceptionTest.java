package org.lorob.testexamples.wiremock.admin.exception;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;


public class WireMockServiceNotFoundExceptionTest {

	@Test
	public void testWireMockServiceIdMismatchExceptionDefault() {
		WireMockServiceNotFoundException exception = new WireMockServiceNotFoundException();
		assertEquals("Failed to find mock service record",exception.getMessage());
	}
	
	@Test
	public void testWireMockServiceIdMismatchException() {
		WireMockServiceNotFoundException exception = new WireMockServiceNotFoundException("message", new Exception("test"));
		assertEquals("message",exception.getMessage());
	}
}
