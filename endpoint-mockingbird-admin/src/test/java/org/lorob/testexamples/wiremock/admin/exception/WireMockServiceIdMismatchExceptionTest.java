package org.lorob.testexamples.wiremock.admin.exception;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;


public class WireMockServiceIdMismatchExceptionTest {

	@Test
	public void testWireMockServiceIdMismatchExceptionDefault() {
		WireMockServiceIdMismatchException exception = new WireMockServiceIdMismatchException();
		assertEquals("Unable to find mock service record",exception.getMessage());
	}
	
	@Test
	public void testWireMockServiceIdMismatchException() {
		WireMockServiceIdMismatchException exception = new WireMockServiceIdMismatchException("message", new Exception("test"));
		assertEquals("message",exception.getMessage());
	}
}
