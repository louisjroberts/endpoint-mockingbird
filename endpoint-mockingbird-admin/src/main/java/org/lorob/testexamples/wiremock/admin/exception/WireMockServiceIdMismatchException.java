package org.lorob.testexamples.wiremock.admin.exception;

public class WireMockServiceIdMismatchException extends Exception {

    private static final long serialVersionUID = -7311408118292778912L;

    public WireMockServiceIdMismatchException() {
        super("Unable to find mock service record");
    }
    
    public WireMockServiceIdMismatchException(String message, Throwable cause) {
        super(message, cause);
    }
}