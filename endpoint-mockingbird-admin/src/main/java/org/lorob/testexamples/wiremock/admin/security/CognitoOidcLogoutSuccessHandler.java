package org.lorob.testexamples.wiremock.admin.security;

import java.net.URI;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.web.util.UriComponentsBuilder;

public class CognitoOidcLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {

	  private final String logoutUrl;
	  private final String clientId;
	  private final String redirectUrl;

	  public CognitoOidcLogoutSuccessHandler(String logoutUrl, String clientId, String redirectUrl) {
	    this.logoutUrl = logoutUrl;
	    this.clientId = clientId;
	    this.redirectUrl = redirectUrl;
	  }

	  @Override
	  protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response,
	                                      Authentication authentication) {
	    return UriComponentsBuilder
	      .fromUri(URI.create(logoutUrl))
	      .queryParam("client_id", clientId)
	      .queryParam("logout_uri", redirectUrl)
	      .encode(StandardCharsets.UTF_8)
	      .build()
	      .toUriString();
	  }
	}
