package org.lorob.testexamples.wiremock.admin.security;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;

import com.nimbusds.jose.shaded.json.JSONArray;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ConditionalOnProperty(name = "mockingbird-admin.security.enabled", havingValue = "true", matchIfMissing = false)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	private final String clientId;
	private final String logoutUrl;
	private final String redirectUri;
	
	public SecurityConfig(
		    @Value("${spring.security.oauth2.client.registration.cognito.clientId}") String clientId,
		    @Value("${cognito.logoutUrl}") String logoutUrl,
		    @Value("${cognito.redirectUri}") String redirectUri
		    ) {
		    this.clientId = clientId;
		    this.logoutUrl = logoutUrl;
		    this.redirectUri = redirectUri;
		  }
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
		authorizeRequests(http.authorizeRequests());
    	http
        .oauth2Login(l -> {
            l.userInfoEndpoint().userAuthoritiesMapper(userAuthoritiesMapper());
        	}
        )
        .logout()
        	.logoutUrl("/logout")
        	.clearAuthentication(true)
        	.logoutSuccessUrl("/")
        	.invalidateHttpSession(true)
        	.logoutSuccessHandler(new CognitoOidcLogoutSuccessHandler(logoutUrl, clientId, redirectUri))
        .and()
        .exceptionHandling().accessDeniedPage("/accessDenied");
    }
    
	private void authorizeRequests(
			ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorizeRequests) {
		authorizeRequests
			.antMatchers("/static/**").permitAll()
			.antMatchers(HttpMethod.GET, "/logoutsuccess").permitAll()
			.antMatchers(HttpMethod.GET, "/accessDenied").permitAll()
			.antMatchers(HttpMethod.GET, "/home").hasRole("MOCKINGBIRD_USER")
			.antMatchers(HttpMethod.GET, "/").hasRole("MOCKINGBIRD_USER")
			.antMatchers(HttpMethod.GET, "/showmocks").hasRole("MOCKINGBIRD_USER")
			.antMatchers(HttpMethod.GET, "/update").hasRole("MOCKINGBIRD_USER")
			.antMatchers(HttpMethod.GET, "/insert").hasRole("MOCKINGBIRD_USER")
			.antMatchers(HttpMethod.GET, "/delete").hasRole("MOCKINGBIRD_USER")
			.antMatchers(HttpMethod.GET, "/showtest").hasRole("MOCKINGBIRD_USER")
			.antMatchers(HttpMethod.GET, "/testmock").hasRole("MOCKINGBIRD_USER")
			.antMatchers(HttpMethod.GET, "/startall").hasRole("MOCKINGBIRD_USER")
			.anyRequest()
	        .authenticated();
	}
       
    @Bean
    public GrantedAuthoritiesMapper userAuthoritiesMapper() {
        return (authorities) -> {
            Set<GrantedAuthority> mappedAuthorities = new HashSet<>();

            @SuppressWarnings("unchecked")
			Optional<OidcUserAuthority> awsAuthority = (Optional<OidcUserAuthority>) authorities.stream()
                    .filter(grantedAuthority -> "ROLE_USER".equals(grantedAuthority.getAuthority()))
                    .findFirst();

            if (awsAuthority.isPresent()) {
            	if(((JSONArray) awsAuthority.get().getAttributes().get("cognito:groups"))!=null) {
            		mappedAuthorities = ((JSONArray) awsAuthority.get().getAttributes().get("cognito:groups")).stream()
                        .map(role -> new SimpleGrantedAuthority("ROLE_" + role))
                        .collect(Collectors.toSet());
            	}
            }

            return mappedAuthorities;
        };
    }
}
