package org.lorob.testexamples.wiremock.admin;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.lorob.testexamples.wiremock.admin.api.WireMockModelProcessor;
import org.lorob.testexamples.wiremock.admin.exception.WireMockServiceIdMismatchException;
import org.lorob.testexamples.wiremock.admin.exception.WireMockServiceNotFoundException;
import org.lorob.testexamples.wiremock.admin.persistence.model.WireMockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.RestAssured;
import io.restassured.response.Response;

@Controller
public class WiremockDBAdminConsoleController {

	private static Logger logger = LoggerFactory.getLogger(WiremockDBAdminConsoleController.class);

	@Value("${spring.application.name:Mockingbird}")
	String applicationName;

	@Value("${spring.application.host:localhost}")
	String testHost;

	@Value("${spring.application.port:}")
	String testPort;

	@Value("${spring.application.version:}")
	String version;

	@Autowired
	private WireMockModelProcessor wireMockModelProcessor;

	@GetMapping("/")
	public String  defaultPage(Model model, Authentication authentication) {
		addToModel(model);
		if (authentication != null && authentication.isAuthenticated()) {
			if (authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("MOCKINGBIRD_USER"))) {
				model.addAttribute("authenticationStatus", "MOCKINGBIRD_USER authenticated");
				model.addAttribute("name", authentication.getName());
		      } else {
		        model.addAttribute("authenticationStatus", "not authenticated");
		        model.addAttribute("name", "");
		      }
		}
		return "forward:/home";
	}
	
	@GetMapping("/accessDenied")
	public String homePage() {
		return "accessDenied";
	}
	
	@GetMapping("/logoutsuccess")
	public String logoutsuccess() {
		return "logoutsuccess";
	}
	
	@GetMapping("/home")
	public String homePage(Model model) {
		addToModel(model);
		return "home";
	}
	
	@GetMapping("/showmocks")
	public String showMocksPage(Model model) {
		logger.debug("/showmocks");
		Iterable<?> wireMockServices = wireMockModelProcessor.findAll();
		model.addAttribute("wireMockServices", wireMockServices);
		addToModel(model);
		model.addAttribute("host", testHost);
		model.addAttribute("port", testPort);
		// Initialise
		wireMockModelProcessor.startAllMocks(testHost);

		return "showmocks";
	}

	@GetMapping("/update")
	public String updateMockPage(Model model, HttpServletRequest httpRequest)
			throws WireMockServiceIdMismatchException, WireMockServiceNotFoundException {
		WireMockService wireMockService = wireMockModelProcessor.update(httpRequest);
		model.addAttribute("host", testHost);
		model.addAttribute("wireMockService", wireMockService);
		addToModel(model);
		return "updatemock";
	}

	@GetMapping("/insert")
	public String insertMockPage(Model model, HttpServletRequest httpRequest) {
		WireMockService newWireMockService = wireMockModelProcessor.insert(httpRequest);
		model.addAttribute("wireMockService", newWireMockService);
		addToModel(model);
		return "insertmock";
	}

	@GetMapping("/delete")
	public String deleteMockPage(Model model, HttpServletRequest httpRequest)
			throws WireMockServiceIdMismatchException, WireMockServiceNotFoundException {
		String id = (String) httpRequest.getParameter("id");
		wireMockModelProcessor.delete(id);

		model.addAttribute("id", id);
		addToModel(model);
		return "deletemock";
	}

	@GetMapping("/showtest")
	public String testMockPage(Model model, HttpServletRequest httpRequest)
			throws WireMockServiceIdMismatchException, WireMockServiceNotFoundException {
		logger.debug("/showtest");
		String id = (String) httpRequest.getParameter("id");
		String host = (String) httpRequest.getParameter("host");
		
		WireMockService wireMockService = wireMockModelProcessor.findOne(id);
		wireMockModelProcessor.startAllMocks(host);

		model.addAttribute("testaction", "show");
		model.addAttribute("host", host);
		model.addAttribute("wireMockService", wireMockService);
		addToModel(model);
		model.addAttribute("port", getPort(wireMockService));
		return "testmock";
	}

	@GetMapping("/testmock")
	public String runTestMockPage(Model model, HttpServletRequest httpRequest)
			throws WireMockServiceIdMismatchException, WireMockServiceNotFoundException {
		logger.debug("/testMock");
		String id = (String) httpRequest.getParameter("id");
		String host = (String) httpRequest.getParameter("host");
		WireMockService wireMockService = wireMockModelProcessor.findOne(id);
		// run test
		model.addAttribute("host", host);
		model.addAttribute("testaction", "test");
		model.addAttribute("wireMockService", wireMockService);
		addToModel(model);
		model.addAttribute("results", testWireMockService(wireMockService, host));

		return "testmock";
	}

	@GetMapping("/startall")
	public String startAll(Model model) {
		addToModel(model);
		wireMockModelProcessor.startAllMocks(testHost);
		return showMocksPage(model);
	}

	// private methods
	private int getPort(WireMockService wireMockService) {
		if (testPort == null || testPort.trim().length() == 0) {
			return wireMockService.getPort();
		} else {
			return Integer.parseInt(testPort);
		}
	}

	@SuppressWarnings("unchecked")
	private WireMockService testWireMockService(WireMockService wireMockService, String host) {
		WireMockService results = new WireMockService();
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> map = new HashMap<String, String>();
		try {
			map = mapper.readValue(wireMockService.getHttpJsonRequestHeaders(), Map.class);
		} catch (JsonProcessingException jme) {
			logger.info("Failed to parse {}", wireMockService.getHttpJsonRequestHeaders(), jme);
		}
		String jsonContentType = MediaType.APPLICATION_JSON_VALUE;
		// need to check the media type to extend to {"Content-Type":"html/text; charset=UTF-8"} for web pages
		// NOTE need a better way to do this
		if(null != results.getHttpResponseFileType() && results.getHttpResponseFileType().contains(MediaType.TEXT_HTML_VALUE)) {
			jsonContentType = MediaType.TEXT_HTML_VALUE;
		}
		String url = "http://" + host + ":" + getPort(wireMockService) + wireMockService.getUrl();
		// 
		if ("GET".equals(wireMockService.getHttpRequestType())) {
			final Response response = RestAssured.given().contentType(jsonContentType)
					.body(wireMockService.getHttpJsonRequest()).headers(map).get(url);
			results.setHttpResponseCode(response.statusCode());
			results.setHttpJsonResponse(response.getBody().asString());
		} else if ("POST".equals(wireMockService.getHttpRequestType())) {
			final Response response = RestAssured.given()
					.contentType(jsonContentType)
					.body(wireMockService.getHttpJsonRequest()).headers(map).post(url);
			results.setHttpResponseCode(response.statusCode());
			results.setHttpJsonResponse(response.getBody().asString());
		} else if ("PATCH".equals(wireMockService.getHttpRequestType())) {
			final Response response = RestAssured.given().contentType(jsonContentType)
					.body(wireMockService.getHttpJsonRequest()).headers(map).patch(url);
			results.setHttpResponseCode(response.statusCode());
			results.setHttpJsonResponse(response.getBody().asString());
		} else if ("DELETE".equals(wireMockService.getHttpRequestType())) {
			final Response response = RestAssured.given().contentType(jsonContentType)
					.body(wireMockService.getHttpJsonRequest()).headers(map).delete(url);
			results.setHttpResponseCode(response.statusCode());
			results.setHttpJsonResponse(response.getBody().asString());
		}

		return results;
	}

	private void addToModel(Model model) {
		model.addAttribute("applicationName", applicationName);
		model.addAttribute("version", version);
	}

}
