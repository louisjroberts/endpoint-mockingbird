package org.lorob.testexamples.wiremock.admin.api;

import javax.validation.Valid;

import org.lorob.testexamples.wiremock.admin.exception.WireMockServiceIdMismatchException;
import org.lorob.testexamples.wiremock.admin.exception.WireMockServiceNotFoundException;
import org.lorob.testexamples.wiremock.admin.persistence.model.WireMockService;
import org.lorob.testexamples.wiremock.admin.repo.WireMockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

// Rest API Controller that communicates to the service repository
@RestController
@RequestMapping("/api/wiremockservice")
public class WireMockAPI {
	@Autowired
	private WireMockRepository wireMockRepository;

	@GetMapping("/fetchAllByRunOrder")
	public Iterable<?> findAllOrderByRunOrder() {
		return wireMockRepository.findAll(Sort.by("RunOrder"));
	}

	@GetMapping
	public Iterable<?> findAll() {
		return wireMockRepository.findAll();
	}

	@GetMapping("/{id}")
	public WireMockService findOne(@PathVariable Long id) throws WireMockServiceNotFoundException {
		return wireMockRepository.findById(id).orElseThrow(WireMockServiceNotFoundException::new);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public WireMockService create(@Valid @RequestBody WireMockService wireMockService) {
		return wireMockRepository.save(wireMockService);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) throws WireMockServiceNotFoundException {
		wireMockRepository.findById(id).orElseThrow(WireMockServiceNotFoundException::new);
		wireMockRepository.deleteById(id);
	}

	@PutMapping("/{id}")
	public WireMockService update(@RequestBody WireMockService wireMockService, @PathVariable Long id)
			throws WireMockServiceIdMismatchException, WireMockServiceNotFoundException {
		if (wireMockService.getId() != id.longValue()) {
			throw new WireMockServiceIdMismatchException();
		}
		wireMockRepository.findById(id).orElseThrow(WireMockServiceNotFoundException::new);
		return wireMockRepository.save(wireMockService);
	}
}
