package org.lorob.testexamples.wiremock.admin.api;

import javax.servlet.http.HttpServletRequest;

import org.lorob.testexamples.wiremock.CustomWireMock;
import org.lorob.testexamples.wiremock.CustomWireMockCallBack;
import org.lorob.testexamples.wiremock.admin.exception.WireMockServiceIdMismatchException;
import org.lorob.testexamples.wiremock.admin.exception.WireMockServiceNotFoundException;
import org.lorob.testexamples.wiremock.admin.persistence.model.WireMockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

// Abstract the calls to the database into a processing class
@Service
public class WireMockModelProcessor implements CustomWireMockCallBack {

	@Autowired
	WireMockAPI serviceRecordController;

	private CustomWireMock customWireMock;

	public Iterable<?> findAll() {
		return serviceRecordController.findAll();
	}

	public WireMockService update(HttpServletRequest httpRequest)
			throws WireMockServiceIdMismatchException, WireMockServiceNotFoundException {
		WireMockService wireMockService = createWireMockService(httpRequest);
		WireMockService createdWireMockService = serviceRecordController.update(wireMockService, wireMockService.getId());
		return createdWireMockService;
	}

	public WireMockService insert(HttpServletRequest httpRequest) {
		WireMockService wireMockService = createWireMockService(httpRequest);
		WireMockService newWireMockService = serviceRecordController.create(wireMockService);

		return newWireMockService;
	}

	public void delete(String id) throws WireMockServiceNotFoundException {
		serviceRecordController.delete(Long.parseLong(id));
	}

	public WireMockService findOne(String id) throws WireMockServiceNotFoundException {
		return serviceRecordController.findOne(Long.parseLong(id));
	}

	@SuppressWarnings("unchecked")
	public void startAllMocks(String host) {

		Iterable<WireMockService> wireMockServices = (Iterable<WireMockService>) serviceRecordController
				.findAllOrderByRunOrder();
		callback();
		for (WireMockService wireMockService : wireMockServices) {
			if (null == getCustomWireMock()) {
				customWireMock = new CustomWireMock(wireMockService.getPort(), host);
				getCustomWireMock().setCallback(this);
			}
			getCustomWireMock().processMockService(wireMockService);
		}

	}
	
	public CustomWireMock getCustomWireMock() {
		return customWireMock;
	}
	
	@Override
	public void callback() {
		if (null != getCustomWireMock()) {
			getCustomWireMock().teardown();
			customWireMock = null;
		}
	}

	private WireMockService createWireMockService(HttpServletRequest httpRequest) {
		String id = (String) httpRequest.getParameter("id");
		String port = (String) httpRequest.getParameter("port");
		String url = (String) httpRequest.getParameter("url");
		String httpJsonRequest = (String) httpRequest.getParameter("httpJsonRequest");
		String httpJsonRequestHeaders = (String) httpRequest.getParameter("httpJsonRequestHeaders");
		String httpJsonResponse = (String) httpRequest.getParameter("httpJsonResponse");
		String httpRequestType = (String) httpRequest.getParameter("httpRequestType");
		String httpResponseCode = (String) httpRequest.getParameter("httpResponseCode");
		String httpResponseFileType = (String) httpRequest.getParameter("httpResponseFileType");
		String runOrder = (String) httpRequest.getParameter("runOrder");

		WireMockService wireMockService = new WireMockService();
		if (id != null) {
			wireMockService.setId(Long.parseLong(id));
		}
		wireMockService.setPort(Integer.parseInt(port));
		wireMockService.setUrl(url);
		wireMockService.setHttpJsonRequest(httpJsonRequest);
		wireMockService.setHttpJsonRequestHeaders(httpJsonRequestHeaders);
		wireMockService.setHttpJsonResponse(httpJsonResponse);
		wireMockService.setHttpRequestType(httpRequestType);
		wireMockService.setHttpResponseCode(Integer.parseInt(httpResponseCode));
		wireMockService.setHttpResponseFileType(httpResponseFileType);
		wireMockService.setRunOrder(Long.parseLong(runOrder));
		return wireMockService;
	}
}
