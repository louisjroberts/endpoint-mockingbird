package org.lorob.testexamples.wiremock.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("org.lorob.testexamples.wiremock.admin.repo") 
@EntityScan("org.lorob.testexamples.wiremock.admin.persistence.model")
@SpringBootApplication
public class WiremockDBAdminConsoleApplication {
    
    public static void main(String[] args) {
            SpringApplication.run(WiremockDBAdminConsoleApplication.class, args);
    }
    
}
