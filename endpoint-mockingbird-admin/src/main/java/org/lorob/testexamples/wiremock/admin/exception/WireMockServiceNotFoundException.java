package org.lorob.testexamples.wiremock.admin.exception;

public class WireMockServiceNotFoundException extends Exception {

    private static final long serialVersionUID = -2628287354698371465L;

    public WireMockServiceNotFoundException() {
        super("Failed to find mock service record");
    }
    
    public WireMockServiceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
