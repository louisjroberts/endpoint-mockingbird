# Example Shell Script File to run endpoint-mockingbird-admin 
# Need to compile the code first using mvn clean install
# This will use the default local H2 database

nohup java -Dspring.jpa.database-platform=org.hibernate.dialect.H2Dialect \
-Dspring.datasource.driver-class-name=org.h2.Driver \
-Dspring.datasource.username=sa \
-Dspring.datasource.url=jdbc:h2:file:./db/mockservice/mockservices \
-jar ./target/endpoint-mockingbird-admin-spring-boot.war > ./output.log 2>&1 &
echo $! > pid.file