# endpoint-mockingbird

endpoint-mockingbird is an application and set of libraries that enables an endpoint to be
 mocked and respond to requests depending on the url format \ url variables \ post data sent
 to it. In this way an intelligent endpoint can mock various responses for unit and 
 integration testing. For example an endpoint passing in valuea in the request will return 
 a HTTP 200 status with a set of JSON data in the body. 
 
 The same endpoint passing in valueb will return a HTTP 403 forbidden response enabling 
 failure routes to be tested.

## Introduction

It is difficult to test applications that have third party endpoints within the code. 
The third party does not always have a test system available. 
You would also wish to test the failure routes from those end points to ensure your code 
is robust and the outcome of success or failure follows the correct paths through the code. 
There are a number of mock endpoint libraries out there which you would have used - 
but quite often this relies on writing a lot of specific mock code which you need to maintain. 
As new scenarios come, you increase the numbers of mocks and the code gets unwieldy. 
You will find that during the test cycle, the tester may wish to run scenarios outside of 
the unit and integration tests already written and this adds further complexity to testing.

endpoint-mockingbird uses a database to store the mocks and comes with a simple editor 
that allows you to add and update additional mocks easily. 
This allows developers and testers to write mocks suitable to their needs. 
In the code, you point the endpoints to the endpoint-mockingbird server machine and port 
and can change scenarios on the fly. 

endpoint-mockingbird allows the user to create endpoints based on the url and also 
the data that is being sent to it. So for example you can set up a situation, 
where depending on data a failure is returned (http status 4xx,5xx codes). 
In this way you can test those failure routes. 
You can also use the data to return different values to the calling service or application.

## Building the application
Run:
`mvn clean install`
from a Git Bash console in the root directory. 
The application will create a the endpoint-mockingbird-runner jar file in the 

`endpoint-mockingbird-runner\target` 

directory and the web application admin war in the 

`endpoint-mockingbird-admin\target` folder

## Running the admin console

The admin console is a spring boot app. From the `endpoint-mockingbird-admin` folder, 
run the shell script from a bash console:

 `run-endpoint-mockingbird-admin.sh`

The configuration is located in the `application.yml` file. 
The default port is 9876. The application can be accessed via the browser. 
The default url is `http://localhost:9876/`

The application uses a H2 database located in the `/endpoint-mockingbird-admin/db/mockservice/` 
subdirectory. There are a number of example endpoints created, which can be deleted. 
Alternatively copy the database (copy the directory mentioned above) into a different directory 
and amend the `spring.datasource.url` value in the application.yml file.

The admin console allows you to add \ amend \ delete endpoints and test their behaviour. It runs all
the endpoints in the database so you can test the endpoints using the admin tool and also via postman.
You will find some postman scripts under the `postman` sub-directory in the `endpoint-mockingbird-runner`
directory.

## Running the headless service

The application is a java jar file. From the `endpoint-mockingbird-runner` folder, run the shell
 script from a bash console:
 `run-endpoint-mockingbird-runner.sh`

The application uses the following program arguments:
`-db [jdbc-url | use-default-db] -host host-name`

These can be changed if you wish to use a different database. For example:

`-db jdbc:h2:file:./db/mockservice/mockservices -host localhost`
or 
`-db use-default-db -host localhost`

The default database is that located in `/endpoint-mockingbird-runner/db/mockservice/mockservices`

There is another shell script called `run-endpoint-mockingbird-admin-db.sh` which attachs to the admin 
database mentioned above. You can use this script against changes made in the admin console.

The endpoints are created ready for a calling application. You can test the results using postman or curl. 
(or the admin console - although you cannot run both at the same time as they are using the same port)

The facility can also be used programmatically. You can start the headless service for integration tests 
and close the service using either a call to the stopservice request of call the `closeservices()` call 
if you have a handle on the created headless service.

The headless service has been created as jar so that a test framework could run the listening enpoint 
mock service during a test cycle and can be closed down by calling the stop server `http://localhost:9999/stopserver` 
via curl. This enables endpoint-mockingbird to be used as part of the test integration pipeline.

## Stopping the Admin \ Headless service
There is a `stop-endpoint-mockingbird-runner.sh` and `stop-endpoint-mockingbird-admin.sh` scripts in the
directories. These only work if you have run the applications from the shell script. They simply kill the 
process id (pid) of the server.

## Testing the headless service
A postman file has been provided called:
`endpoint-mockingbird.postman_collection.json`
located in 
`endpoint-mockingbird\endpoint-mockingbird-runner\src\test\resources\postman`
Import this into postman. Their are a number of requests that test the GET, POST, DELETE and PATCH commands. 
There is also a 'hello world' request to check that the service is running and a 'stop server' which will 
stop the application from running. The same can be achieved by calling the `closeServices()` command. 

## The database
A H2 database is provided. You cannot run the admin console and runner at the same time against the 
database as the ports will conflict. It is best to have the admin console separate and after amendments 
have been made, copy the database to where the runners will pick up the database. 
You can also create multiple databases and admin consoles, if you wish to create endpoints specific to an 
application. 

## Getting endpoint-mockingbird working on AWS Quickly (i.e. no pipeline)
This was a bit challenging.

1) Create a posgres RDS database. Set the postgres database to be public accessible. 
Create a default security group. Note down the endpoint (AWS_POSTGRES_ENDPOINT), database name (DB_NAME), 
user (DB_USER), password (DB_PASSWORD) for the database. 
2) Use a database tool to Log onto the postgres database using the AWS_POSTGRES_ENDPOINT, DB_NAME,DB_USER, 
DB_PASSWORD from above. The format is 
`jdbc:postgresql://AWS_POSTGRES_ENDPOINT:5432/DB_NAME?user=DB_USER&password=DB_PASSWORD`
3) Create the wire_mock_services database table. 
The script is in endpoint-mockingbird\endpoint-mockingbird-runner\src\main\resources\build-db-postgres.sql
4) Create an EC2 instance.
5) Amend the security group inbound settings to accept customer TCP on ports 9876 and 9999 for source 
0.0.0.0/0. You can amend this to specific ports later once working
6) Amend the security group inbound settings to accept postgres DB port 5432 for the postgres security group 
that gets set up with your postgres database and 0.0.0.0/0. 
Not sure why the port is needed for the jdbc driver to work. Adding the security group did not work.
7) On EC2 instance install maven (see https://docs.aws.amazon.com/neptune/latest/userguide/iam-auth-connect-prerq.html)
8) Fetch the code from the open source project  
`git clone git@gitlab.com:louisjroberts/endpoint-mockingbird.git
9) Compile the code using 
`mvn clean install.
If you hit a heap space issue type the following on the command line and try again:
`export MAVEN_OPTS=-Xmx1024m

### To run the admin console
1) Set up postgres variables that are listed in the `run-endpoint-mockingbird-admin-postgres.sh` script in your environment.
2) Go to the endpoint-mockingbird-admin directory and run the `run-endpoint-mockingbird-admin-postgres.sh`:
3) If all is well then the admin application will run and is accessible via the EC2 IP and port. i.e. http://1.2.3.4:9876/
   
### To run the runner
1) Set up postgres variables that are listed in the `run-endpoint-mockingbird-runner-postgres.sh` script in your environment.
2) Go to the `endpoint-mockingbird-runner directory` and run the `run-endpoint-mockingbird-runner-postgres.sh` script
3) If all is well then the runner application will run and is accessible via the EC2 IP and port. i.e. http://1.2.3.4:9999/

You can't run the runner and admin at the same time on the same machine. If you do you can't test the changes 
in the admin console as the port will already be used by the runner.
The runner only reads the database once on startup, so changes to the postgres database will only happen when 
the runner is restarted. It is best to use the admin console and only use the runner if you are not making any
more changes.

##MySQL Version
There are scripts to set up a MySql version of the database under endpoint-mockingbird\endpoint-mockingbird-runner\src\main\resources\build-db-mysql.sql
A docker image was used which causes problems for connecting. The docker image has its own IP address and to connect this needed to be added to the user
credentials to allow connecting.
The connection configuration file is found at \endpoint-mockingbird\endpoint-mockingbird-admin\src\main\resources\application-mysql.yml
When running remember to add the following program arguments:
--spring.profiles.active=mysql --server.port=9876 --spring.datasource.url=jdbc:mysql://localhost:3306/mockservicedb --spring.datasource.username=appuser --spring.datasource.password=password1
which correspond to the user and password created in the scripts

## Securing the Admin console with Cognito
1) Set up a Cognito Pool and a Group Called MOCKINGBIRD_USER. See https://www.baeldung.com/spring-security-oauth-cognito, https://blog.devgenius.io/example-of-spring-boot-application-authentication-with-aws-cognito-55ce34fa53dc
and https://rieckpil.de/thymeleaf-oauth2-login-with-spring-security-and-aws-cognito/ for guides to setting up cognito and how the security works within the endpoint-mockingbird application
2) Edit the commented out environment variables in the application-progres.yml. OR copy this file rename and upon launch change the spring.active.profile
to the new named configuration. 
3) When you launch the application, when you navigate to the url, you will be asked to authenticate or create a new user. 
4) Manual Step - Once the user is created you will need to add the user in cognito to the MOCKINGBIRD_USER group you created. (You don't want anybody just self registering for the service)


## Getting endpoint-mockingbird setup in your own pipeline - UNDER CONSTRUCTION
If you look at the `.gitlab-ci.yml` file there are stages in the deployment where the code is copied from 
the GitLab repository after building and copies the newly versioned file and copies the file without versioned
information into the root directory of an S3 bucket. This is to allow integration with an AWS pipeline.

1) Mirror the project in your GitLab repository
2) Follow the steps 1-7 from `Getting endpoint-mockingbird working on AWS Quickly`
3) Set up an S3 bucket to copy the deployment code into
4) Open up the `.gitlab-ci.yml file`. You will see the variables to access your S3 bucker. 
Add these to your mirror GitLab Environment. (Go to Settings->CI\CD->Variables)
5) Create an EC2 instance to deploy to.
6) Create a CodeDeploy that pulls your code from your S3 bucket
7) Create a CodePipeline that watches your CodeDeploy and will react when new code is copied in.
8) Set up CodePipeline to stop the server (using `stop-endpoint-mockingbird-admin.sh`) on the EC2 instance, 
deploys to the EC2 instance and restarts using the `run-endpoint-mockingbird-admin-postgres.sh` script.

I will update the instructions when I figure this out !





