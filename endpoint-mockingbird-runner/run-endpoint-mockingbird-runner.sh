# Example Shell Script File to run endpoint-mockingbird-runner
# Need to compile the code first using mvn clean install
# and possibly rename the jar
nohup java -cp "./target/endpoint-mockingbird-runner-0.0.14.jar;./target/libs;../lib/h2-1.4.199.jar" \
org.lorob.testexamples.wiremock.DataDrivenMockApplication -db use-default-db -host localhost -driver org.h2.Driver > ./output.log 2>&1 &
echo $! > pid.file