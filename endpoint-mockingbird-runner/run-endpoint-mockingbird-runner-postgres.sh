# Example Shell Script File to run endpoint-mockingbird-runner
# Need to compile the code first using mvn clean install
# and possibly rename the jar
# You will need to set up your postgres URL as POSTGRES_MOCKINGBIRD_FULL_URL or replace in the script below
# Note the full url is in the format jdbc:postgresql://POSTGRES_DB_URL:5432/postgres?user=POSTGRES_USERNAME&password=POSTGRES_PASSWORD
# The MOCKINGBIRD_SERVER value can be localhost or the IP\URL address of the server that is running the admin console.

nohup java -Dserver.port=9999 -Dspring.jpa.database-platform=org.hibernate.dialect.PostgreSQL10Dialect \
-cp "./target/endpoint-mockingbird-runner-0.0.14.jar;./target/libs;../lib/postgresql-42.2.14.jar" \
org.lorob.testexamples.wiremock.DataDrivenMockApplication -db $POSTGRES_MOCKINGBIRD_FULL_URL -host $MOCKINGBIRD_SERVER -driver org.postgresql.Driver -port 9999 > ./output.log 2>&1 &
echo $! > pid.file
