package org.lorob.testexamples.wiremock.db;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.lorob.testexamples.wiremock.admin.persistence.model.WireMockService;

public class WireMockDBOverriddenPortTest {

	String dbUrlStart = "jdbc:h2:file:";
	String dbUrlEnd = "mockservices";

	private static String APPLICATION_JSON = "application/json";

	WireMockDB db;

	private String getConnectionURL() {
		String path = "db/mockservice";
		File file = new File(path);
		String url = dbUrlStart + file.getAbsolutePath() + "/" + dbUrlEnd;
		url = url.replace('\\', '/');
		return url;
	}

	private String getDriver() {
		return "org.h2.Driver";
	}

	private String getPort() {
		return "9999";
	}

	@BeforeEach
	public void setup() throws ClassNotFoundException, SQLException {
		db = new WireMockDB(getConnectionURL(), getDriver(), getPort());
		db.initDatabase();
	}

	@AfterEach
	public void teardown() throws SQLException {
		db.closeDatabase();
	}

	@Test
	public void testGetMaxRecordId() {
		int noRecords = db.getMaxRecordId();
		assertTrue(noRecords > 0);
	}

	@Test
	public void testReadServices() {
		HashMap<Long, WireMockService> records = db.readServices();
		assertTrue(records.size() > 0);
	}

	@Test
	public void testInsertAndDeleteRecord() {
		int noRecords = db.getMaxRecordId();
		int responseCode = 200;
		int port = 2222;
		noRecords++;
		HashMap<String, String> record = createDBRecord(noRecords, responseCode, port);
		boolean updated = db.createDatabaseRecord(record);
		assertTrue(updated);

		long newRecordId = db.getMaxRecordId();
		assertEquals(noRecords, newRecordId);

		HashMap<Long, WireMockService> records = db.readServices();
		WireMockService thisRecord = records.get(newRecordId);
		assertEquals(thisRecord.getId(), newRecordId);
		/*
		 * "ID","PORT","URL","HTTP_REQUEST_TYPE","HTTP_JSON_REQUEST",
		 * "HTTP_JSON_REQUEST_HEADERS","HTTP_JSON_RESPONSE","HTTP_RESPONSE_CODE",
		 * "HTTP_RESPONSE_FILE_TYPE","RUN_ORDER"
		 */
		assertEquals(newRecordId, thisRecord.getId());
		assertEquals(Integer.parseInt(getPort()), thisRecord.getPort());
		assertEquals(WireMockDB.DB_PARAMETERS[2], thisRecord.getUrl());
		assertEquals(WireMockDB.DB_PARAMETERS[3], thisRecord.getHttpRequestType());
		assertEquals(WireMockDB.DB_PARAMETERS[4], thisRecord.getHttpJsonRequest());
		assertEquals(WireMockDB.DB_PARAMETERS[5], thisRecord.getHttpJsonRequestHeaders());
		assertEquals(WireMockDB.DB_PARAMETERS[6], thisRecord.getHttpJsonResponse());
		assertEquals(responseCode, thisRecord.getHttpResponseCode());
		assertEquals(APPLICATION_JSON, thisRecord.getHttpResponseFileType());
		assertEquals(newRecordId, thisRecord.getRunOrder());

		// test single record read
		WireMockService singleRecord = db.readServiceAtIndex(newRecordId);
		assertEquals(newRecordId, singleRecord.getId());
		assertEquals(Integer.parseInt(getPort()), singleRecord.getPort());
		assertEquals(WireMockDB.DB_PARAMETERS[2], singleRecord.getUrl());
		assertEquals(WireMockDB.DB_PARAMETERS[3], singleRecord.getHttpRequestType());
		assertEquals(WireMockDB.DB_PARAMETERS[4], singleRecord.getHttpJsonRequest());
		assertEquals(WireMockDB.DB_PARAMETERS[5], singleRecord.getHttpJsonRequestHeaders());
		assertEquals(WireMockDB.DB_PARAMETERS[6], singleRecord.getHttpJsonResponse());
		assertEquals(responseCode, singleRecord.getHttpResponseCode());
		assertEquals(APPLICATION_JSON, thisRecord.getHttpResponseFileType());
		assertEquals(newRecordId, thisRecord.getRunOrder());

		updated = db.deleteDatabaseRecord(newRecordId);
		assertTrue(updated);
		noRecords = db.getMaxRecordId();
		assertTrue(noRecords == (newRecordId - 1));
	}

	private HashMap<String, String> createDBRecord(int index, int status, int port) {
		HashMap<String, String> record = new HashMap<String, String>();
		record.put(WireMockDB.DB_PARAMETERS[0], Integer.toString(index));
		record.put(WireMockDB.DB_PARAMETERS[1], Integer.toString(port));
		record.put(WireMockDB.DB_PARAMETERS[2], WireMockDB.DB_PARAMETERS[2]);
		record.put(WireMockDB.DB_PARAMETERS[3], WireMockDB.DB_PARAMETERS[3]);
		record.put(WireMockDB.DB_PARAMETERS[4], WireMockDB.DB_PARAMETERS[4]);
		record.put(WireMockDB.DB_PARAMETERS[5], WireMockDB.DB_PARAMETERS[5]);
		record.put(WireMockDB.DB_PARAMETERS[6], WireMockDB.DB_PARAMETERS[6]);
		record.put(WireMockDB.DB_PARAMETERS[7], Integer.toString(status));
		record.put(WireMockDB.DB_PARAMETERS[8], APPLICATION_JSON);
		record.put(WireMockDB.DB_PARAMETERS[9], Long.toString(index));
		return record;
	}

	@Test
	public void testUpdateRecord() {
		int noRecords = db.getMaxRecordId();
		int port = 2222;
		noRecords++;
		HashMap<String, String> record = createDBRecord(noRecords, 200, port);
		boolean updated = db.createDatabaseRecord(record);
		assertTrue(updated);

		long newRecordId = db.getMaxRecordId();
		assertEquals(noRecords, newRecordId);
		HashMap<Long, WireMockService> records = db.readServices();
		WireMockService thisRecord = records.get(newRecordId);
		long recordId = thisRecord.getId();

		HashMap<String, String> updateRecord = new HashMap<String, String>();
		updateRecord.put(WireMockDB.DB_PARAMETERS[0], Long.toString(recordId));
		updateRecord.put(WireMockDB.DB_PARAMETERS[1], "1111");
		updateRecord.put(WireMockDB.DB_PARAMETERS[2], thisRecord.getUrl() + "**");
		updateRecord.put(WireMockDB.DB_PARAMETERS[3], thisRecord.getHttpJsonRequest() + "**");
		updateRecord.put(WireMockDB.DB_PARAMETERS[4], thisRecord.getHttpJsonRequestHeaders() + "**");
		updateRecord.put(WireMockDB.DB_PARAMETERS[5], thisRecord.getHttpJsonResponse() + "**");
		updateRecord.put(WireMockDB.DB_PARAMETERS[6], thisRecord.getHttpRequestType() + "**");
		updateRecord.put(WireMockDB.DB_PARAMETERS[7], "500");
		updateRecord.put(WireMockDB.DB_PARAMETERS[8], APPLICATION_JSON);
		updateRecord.put(WireMockDB.DB_PARAMETERS[9], Long.toString(recordId + 1));
		boolean hasUpdated = db.updateDatabaseRecord(updateRecord);
		assertTrue(hasUpdated);

		records = db.readServices();
		WireMockService amendedRecord = records.get(newRecordId);
		assertEquals(Integer.parseInt(getPort()), amendedRecord.getPort());
		assertTrue(amendedRecord.getUrl().endsWith("**"));
		assertTrue(amendedRecord.getHttpJsonRequest().endsWith("**"));
		assertTrue(amendedRecord.getHttpJsonRequestHeaders().endsWith("**"));
		assertTrue(amendedRecord.getHttpJsonResponse().endsWith("**"));
		assertTrue(amendedRecord.getHttpRequestType().endsWith("**"));
		assertEquals(500, amendedRecord.getHttpResponseCode());
		assertEquals(APPLICATION_JSON, amendedRecord.getHttpResponseFileType());
		assertEquals((recordId + 1), amendedRecord.getRunOrder());

		updated = db.deleteDatabaseRecord(newRecordId);
		assertTrue(updated);
		noRecords = db.getMaxRecordId();
		assertTrue(noRecords == (newRecordId - 1));
	}

}
