package org.lorob.testexamples.wiremock;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class CustomWireMockTest implements CustomWireMockCallBack {
	
	static CustomWireMock customWireMock;
	boolean callbackCalled;
	
	@BeforeAll
	public static void setup() {
		customWireMock = new CustomWireMock(9999,"localhost");
	}
	
	@AfterAll
	public static void teardown() {
		customWireMock.teardown();
	}
	
	@Test
	public void testCallback() {
		customWireMock.setCallback(this);
		CustomWireMockCallBack callBack = customWireMock.getCallback();
		assertNotNull(callBack);
		assertTrue(callBack instanceof CustomWireMockTest);
	}
	
	@Test
	public void testGetMappedValue() {
		String jsonString = "{\"Content-Type\":\"application/json; charset=UTF-8\"}";
		String foundValue = customWireMock.getMappedValue("Content-Type", jsonString);
		assertEquals("application/json; charset=UTF-8", foundValue);
	}
	
	@Test
	public void testGetMappedValueCantBeFound() {
		String jsonString = "{\"Content-Type\":\"application/json; charset=UTF-8\"}";
		String foundValue = customWireMock.getMappedValue("missing", jsonString);
		assertEquals("", foundValue);
	}
	
	@Test
	public void testGetMappedValueWithUnparsableJsonReturnEmptyString () {
		String jsonString = "{\"Content-Type\":\"application/json; charset=UTF-8\"";
		String foundValue = customWireMock.getMappedValue("Content-Type", jsonString);
		assertEquals("", foundValue);
	}
	
	@Test
	public void testKillApplication() {
		customWireMock.setCallback(this);
		callbackCalled = false;
		customWireMock.killApplication();
		assertTrue(callbackCalled);
	}

	@Override
	public void callback() {
		// implementing for testing
		callbackCalled = true;
	}
}
