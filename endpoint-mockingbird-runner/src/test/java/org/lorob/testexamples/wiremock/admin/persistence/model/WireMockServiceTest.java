package org.lorob.testexamples.wiremock.admin.persistence.model;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.lorob.testexamples.utils.PojoTestUtils;

/**
 * WireMockServiceRecord Test
 * 
 * @author lorob
 *
 */
public class WireMockServiceTest {
	
	@Test
	public void testSettersAndGettersTests() {
		PojoTestUtils.validateAccessors(WireMockService.class);
	}

	
	@Test
	public void testEqualsAndHashMethodsTests() {
		PojoTestUtils.validateEquals(WireMockService.class);
	}
	
	@Test
	public void testToStringTest() {
	    WireMockService simplePOJO=new WireMockService();
		assertNotNull(simplePOJO.toString());
	}
	
}
