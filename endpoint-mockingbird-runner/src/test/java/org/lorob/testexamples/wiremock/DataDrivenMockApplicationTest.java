package org.lorob.testexamples.wiremock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import io.restassured.RestAssured;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import io.restassured.response.Response;
import net.minidev.json.JSONObject;



/*
 * DB Data
 * 1   9999   /testservice    GET     {"Content-Type":"application/json; charset=UTF-8"} OK  200 1
 * 2   9999   /testservice?value=1    GET     {"Content-Type":"application/json; charset=UTF-8"} {"status":"FAIL"}   403 2
 * 3   9999   /testservice?value=2    GET     {"Content-Type":"application/json; charset=UTF-8"} {"status":"FAIL"}   500 3
 * 4   9999   /testservicepost    POST    {"value":"1"}   {"Content-Type":"application/json; charset=UTF-8"} {"status":"OK"} 200 4
 * 5   9999   /testservicepost    POST    {"value":"2"}   {"Content-Type":"application/json; charset=UTF-8"} {"status":"FAIL"}   403 5
 * 6   9999   /testservice?value=3    GET     {"Content-Type":"application/json; charset=UTF-8"} {"status":"OK"} 200 6
 */
@ActiveProfiles("test")
public class DataDrivenMockApplicationTest {
    
    private static String connectionString = DataDrivenMockApplication.sDefaultURL;
    private static DataDrivenMockApplication mockedService; 
    private static String URL = "http://127.0.0.1:9999";
    
    @BeforeAll
    public static void setup() throws Exception {
        mockedService=new DataDrivenMockApplication(connectionString, DataDrivenMockApplication.sLocalhost, "org.h2.Driver", "9999");
        mockedService.runMocks();
    }
    
    @AfterAll
    public static void close() {
    	mockedService.closeServices();
    	RestAssuredMockMvc.reset();
    }

    @Test
    public void testDataDrivenMockApplicationGetOkIntegrationTest() 
        throws Exception {
        final Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .get(URL + "/testservice");
        
        assertEquals(200, response.getStatusCode());
    }
    
    @Test
    public void testDataDrivenMockApplicationGetForbiddenIntegrationTest() 
        throws Exception {
        final Response response = RestAssured.given()
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .get(URL + "/testservice?value=1");
        assertEquals(403, response.getStatusCode());
    }
    
    @Test
    public void testDataDrivenMockApplicationGetInternalServerErrorIntegrationTest() 
        throws Exception {
        final Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .get(URL + "/testservice?value=2");
        assertEquals(500, response.getStatusCode());
    }
    
    @Test
    public void testDataDrivenMockApplicationGetWithValueOkIntegrationTest() 
        throws Exception {
        final Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .get(URL + "/testservice?value=3");
        
        assertEquals(200, response.getStatusCode());
        assertEquals("{\"status\":\"OK\"}",response.getBody().asString());
    }
    
    @Test
    public void testDataDrivenMockApplicationPostOkIntegrationTest() 
        throws Exception {
        JSONObject body = new JSONObject();
        body.put("value", "1");
        final Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(body.toJSONString())
                .post(URL + "/testservicepost");
        
        assertEquals(200, response.getStatusCode());
        assertEquals("{\"status\":\"OK\"}",response.getBody().asString());
    }
    
    @Test
    public void testDataDrivenMockApplicationPostForbiddenIntegrationTest() 
        throws Exception {
        JSONObject body = new JSONObject();
        body.put("value", "2");
        final Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(body.toJSONString())
                .post(URL + "/testservicepost");
        assertEquals(403, response.getStatusCode());
    }     
    
    @Test
    public void testParameterParsingDefault() {
        String[] args = {"-db","use-default-db","-host","localhost","-driver","org.h2.Driver","port","9999"};
        HashMap<String,String> processedArguments =DataDrivenMockApplication.processArguments(args);
        String connectionString = processedArguments.get("db");
        String host = processedArguments.get("host");
        String driver = processedArguments.get("driver");
        String port = processedArguments.get("port");
        assertTrue("true".equals(processedArguments.get("status")));
        assertTrue(driver.equals(processedArguments.get("driver")));
        assertTrue(port.equals(processedArguments.get("port")));
        assertEquals(DataDrivenMockApplication.sDefaultURL,connectionString);
        assertEquals(DataDrivenMockApplication.sLocalhost,host);
    }
    
}
