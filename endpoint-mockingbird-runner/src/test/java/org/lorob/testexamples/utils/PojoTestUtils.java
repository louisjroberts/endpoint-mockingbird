package org.lorob.testexamples.utils;

import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

/**
 * Source - https://reliablesoftwareblog.wordpress.com/2016/04/28/achieving-100-code-coverage-testing-getters-and-setters/
 * @author lorob
 *
 */
public class PojoTestUtils {
		 
	    private static final Validator ACCESSOR_VALIDATOR = ValidatorBuilder.create()
	                                                    .with(new GetterTester())
	                                                    .with(new SetterTester())
	                                                    .build();
	    
	    /**
	     * Call this in your POJO tests to test the getters and setters
	     * @param clazz
	     */
	    public static void validateAccessors(final Class<?> clazz) {
	        ACCESSOR_VALIDATOR.validate(PojoClassFactory.getPojoClass(clazz));
	    }
	    
	    
	    /**
	     * Call this to validate equals and hashcode
	     * @param clazz
	     */
	    public static void validateEquals(final Class<?> clazz) {
	    	EqualsVerifier.forClass(clazz)
			.usingGetClass()
			.suppress(Warning.NONFINAL_FIELDS)
			.verify();
	    }
	    
}
