package org.lorob.testexamples.wiremock;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.delete;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.patch;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.lorob.testexamples.wiremock.admin.persistence.model.WireMockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.MappingBuilder;

/**
 * CustomWireMock Creates custom wiremock endpoints from the handed records
 * 
 * @author lorob
 *
 */
public class CustomWireMock extends WireMockBase {

	private static Logger logger = LoggerFactory.getLogger(CustomWireMock.class);

	private CustomWireMockCallBack callback;

	public CustomWireMock(int port, String baseUrl) {
		super(port, baseUrl);
	}

	public void processMockService(WireMockService record) {
		// NOTE - Need to think about the header mapping here
		// as can have several values other than Content-Type
		// each needing a new .withHeader call

		if ("GET".equals(record.getHttpRequestType())) {
			processGet(record);
		} else if ("POST".equals(record.getHttpRequestType())) {
			processPost(record);
		} else if ("DELETE".equals(record.getHttpRequestType())) {
			processDelete(record);
		} else if ("PATCH".equals(record.getHttpRequestType())) {
			processPatch(record);
		}
	}

	public String getMappedValue(String valueToFind, String jsonString) {
		Map<?, ?> map = parseJsonString(jsonString);
		String mappedValue = (String) map.get(valueToFind);
		if (mappedValue == null) {
			return "";
		}
		return mappedValue;
	}

	public CustomWireMockCallBack getCallback() {
		return callback;
	}

	public void setCallback(CustomWireMockCallBack callback) {
		this.callback = callback;
	}

	public void killApplication() {
		if (callback != null) {
			callback.callback();
		}
	}
	
	/**
	 * Parse the passed in Json String Used for extracting into a map
	 * 
	 * @param jsonString
	 * @return
	 */
	private Map<?, ?> parseJsonString(String jsonString) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			Map<?, ?> map = mapper.readValue(jsonString, Map.class);
			return map;
		} catch (Exception e) {
			logger.error("Unable to parse:{} \nError:", jsonString, e);
			return new HashMap<>();
		}
	}
	
	private void callStubMapping(String bodyMatch, MappingBuilder stubMapping) {
		if (bodyMatch != null && bodyMatch.length() > 0) {
			stubMapping = stubMapping.withRequestBody(equalToJson(bodyMatch, true, true));
			stubFor(stubMapping);
		} else {
			// do not match the body
			stubFor(stubMapping);
		}
	}

	private void processGet(WireMockService record) {
		logger.debug("processing GET for {}", record);
		MappingBuilder stubMapping = get(urlEqualTo(record.getUrl())).willReturn(aResponse()
				.withStatus(record.getHttpResponseCode()).withHeader("Content-Type", record.getHttpResponseFileType())
				.withBody(record.getHttpJsonResponse()));
		stubMapping = processHeaders(stubMapping, record);
		stubFor(stubMapping);
	}

	private void processPost(WireMockService record) {
		logger.debug("processing POST for {}", record);
		String bodyMatch = record.getHttpJsonRequest();
		MappingBuilder stubMapping = post(urlEqualTo(record.getUrl())).willReturn(aResponse()
				.withStatus(record.getHttpResponseCode()).withHeader("Content-Type", record.getHttpResponseFileType())
				.withBody(record.getHttpJsonResponse()));
		stubMapping = processHeaders(stubMapping, record);
		callStubMapping(bodyMatch, stubMapping);
	}

	/**
	 * Add the headers from the record to the MappingBuilder stub and return the
	 * amended stub Possibly the return is superfluous
	 * 
	 * @param inBuilder
	 * @param record
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private MappingBuilder processHeaders(MappingBuilder inBuilder, WireMockService record) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> map = new HashMap<String, String>();
		try {
			if (null != record.getHttpJsonRequestHeaders() && record.getHttpJsonRequestHeaders().length() > 0) {
				map = mapper.readValue(record.getHttpJsonRequestHeaders(), Map.class);
			}
		} catch (JsonProcessingException jme) {
			logger.info("Failed to parse {}", record.getHttpJsonRequestHeaders(), jme);
			return inBuilder;
		}
		Iterator<String> iter = map.keySet().iterator();
		while (iter.hasNext()) {
			String key = iter.next();
			String value = map.get(key);
			inBuilder = inBuilder.withHeader(key, equalTo(value));
		}

		return inBuilder;
	}

	private void processPatch(WireMockService record) {
		logger.debug("processing PATCH for {}", record);
		String bodyMatch = record.getHttpJsonRequest();
		MappingBuilder stubMapping = patch(urlEqualTo(record.getUrl())).willReturn(aResponse()
				.withStatus(record.getHttpResponseCode()).withHeader("Content-Type", record.getHttpResponseFileType())
				.withBody(record.getHttpJsonResponse()));
		stubMapping = processHeaders(stubMapping, record);
		callStubMapping(bodyMatch, stubMapping);
	}

	private void processDelete(WireMockService record) {
		logger.debug("processing DELETE for {}", record);
		String bodyMatch = record.getHttpJsonRequest();
		MappingBuilder stubMapping = delete(urlEqualTo(record.getUrl())).willReturn(aResponse()
				.withStatus(record.getHttpResponseCode()).withHeader("Content-Type", record.getHttpResponseFileType())
				.withBody(record.getHttpJsonResponse()));
		stubMapping = processHeaders(stubMapping, record);
		callStubMapping(bodyMatch, stubMapping);
	}

}
