package org.lorob.testexamples.wiremock;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

import java.util.List;

import javax.swing.SwingWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.admin.model.GetScenariosResult;
import com.github.tomakehurst.wiremock.stubbing.Scenario;

/**
 * Base Class for running a WireMock Server.
 * To use:
 * Extend this class and pass in a port and baseUrl. 
 * If nothing is passed localhost:8089 is used
 * 
 * Example Extension
 * 
 * ExtendedWireMock extends ExtendedWireMock { 
 * ...
 *  ExtendedWireMock mockedService=new AllPayWireMock(port,baseUrl);
 *  mockedService.createMyOwnStub()
 *  ...
 *  public void createMyOwnStubs(){
 *  	stubFor(get(urlEqualTo(
 *      ...
 *  }
 * ...
 * 
 * The default mocked responses are:
 * http://localhost:8089/helloworld - this responds with a message with the url and port
 * http://localhost:8089/stopserver - this stops the server
 * You can create your own stub match \ responses in your own class. 
 * @author lorob
 *
 */
public class WireMockBase {
	
	private static Logger logger = LoggerFactory.getLogger(WireMockBase.class);
	
	static int defaultPort=8089;
	int port;
	String baseUrl;
	String schemeCode;
	
	WireMockServer wireMockServer;
	
	SwingWorker<?, ?> worker;
	boolean killWorker=false;
	static String KILL_SERVER="KillCalled";
	
	public WireMockBase() {
		this(defaultPort,"localhost");
	}
	
	public WireMockServer getWireMockServer() {
	    return wireMockServer;
	}
	
	/**
	 * Port and base url to use to listen on. default is 8089 localhost
	 * @param port
	 * @param baseUrl
	 */
	public WireMockBase(int port,String baseUrl,String... extensions) {
		this.setPort(port);
		this.setBaseUrl(baseUrl);
		this.setup(extensions);
		this.setUpHelloWorldEndpoint();
		this.setUpStopEndpoint();
		this.setupScenarioListener();
	}
	
	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getSchemeCode() {
		return schemeCode;
	}

	public void setSchemeCode(String schemeCode) {
		this.schemeCode = schemeCode;
	}

	public void setup(String... extensions) {
	    if(extensions == null) {
	        wireMockServer = new WireMockServer(options().port(this.getPort()));
	    }
	    else {
	        wireMockServer = new WireMockServer(options().port(this.getPort()).extensions(extensions));
	    }
	    wireMockServer.start();
	    configureFor(this.getBaseUrl(), wireMockServer.port());
	}
	
	public void teardown() {
		logger.info("Killing server");
		wireMockServer.stop();
	}
	
	/**
	 * Listens for http://localhost:8089/helloworld to test that the service is running
	 */
	public void setUpHelloWorldEndpoint() {
		// This will stub out the fake service
		stubFor(get(urlEqualTo("/helloworld"))
				.withHeader("Content-Type", equalTo("application/json"))
	            .willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
	            .withBody("Hello World. Wire Mock is working on Url: "+this.getBaseUrl()+" port: "+this.getPort())));
	}
	
	/**
	 * Listens for http://localhost:8089/stopserver to kill the server
	 */
	public void setUpStopEndpoint() {
		stubFor(get(urlEqualTo("/stopserver"))
				.withHeader("Content-Type", equalTo("application/json"))
	            .willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
	            .withBody("Stopping mock server on : "+this.getBaseUrl()+" port: "+this.getPort()))
	            .inScenario("killserver")
	            .willSetStateTo(KILL_SERVER)
				);
		
	}
	
	/**
	 * Override this to do something different
	 */
	public void killApplication() {
		logger.info("Kill Application Called");
		//System.exit(0);
	}
	
	
	/**
	 * This method sets up a listener thread that checks whether the stopserver url has been called.
	 * stopServer changes the scenario to KillServer which this thread is waiting for to close down the WireMock
	 * Server
	 */
	public void setupScenarioListener() {
		worker = new SwingWorker<Object, Object>() {
			@Override
			protected Object doInBackground() throws Exception {
				logger.info("Listener Called");
				while(!killWorker) {
					//Thread.sleep(100);
					GetScenariosResult allScenariosResult =  wireMockServer.getAllScenarios();
					if(allScenariosResult!=null) {
						List<Scenario>allScenarios=allScenariosResult.getScenarios();
						for(int i=0;i<allScenarios.size();i++) {
							if(allScenarios.get(i).getState().equals(KILL_SERVER)) {
								killWorker=true;
							}
						}
							
					}
					
				}
				teardown();
				killApplication();
				return null;
			}
			
        };
	    worker.execute();
	}
	
}
