package org.lorob.testexamples.wiremock.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import org.lorob.testexamples.wiremock.admin.persistence.model.WireMockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * H2 Database container
 * Houses connection setup, close and SQL to create a new instance
 * and to query information
 * @author lorob
 *
 */
public class WireMockDB {
    private static Logger logger=LoggerFactory.getLogger(WireMockDB.class);
    
    private Connection connection;
    private String dbUrl;
    private String dbDriver;
    private String dbPort;
    
    // Strings for update and database column names
    public static String[] DB_PARAMETERS = {"ID","PORT","URL","HTTP_REQUEST_TYPE","HTTP_JSON_REQUEST", 
                                            "HTTP_JSON_REQUEST_HEADERS","HTTP_JSON_RESPONSE","HTTP_RESPONSE_CODE","HTTP_RESPONSE_FILE_TYPE","RUN_ORDER"};
    
    
    /**
     * Create a H2 DB
     * @param dbUrl - URL of database connection.
     */
    public WireMockDB(String dbUrl, String dbDriver, String port) {
        this.dbUrl = dbUrl;
        this.dbDriver = dbDriver;
        this.dbPort = port;
    }
    
    /**
     * Initialise Database and create connection 
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public void initDatabase() throws ClassNotFoundException, SQLException{
        logger.info("Getting dbDriver to :{}",dbDriver);
        Class.forName(dbDriver);
        connection = getConnection(); 
    }
    
    /**
     * Create an empty database table
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public void createEmptyDatabase() {
        logger.debug("createEmptyDatabase");
        try (Statement statement = connection.createStatement()) {
            statement.execute("CREATE TABLE wire_mock_service (ID INT auto_increment NOT NULL, "
                    + "PORT INT NOT NULL,"
                    + "URL VARCHAR(512) NOT NULL,"
                    + "HTTP_REQUEST_TYPE VARCHAR(128) NOT NULL,"
                    + "HTTP_JSON_REQUEST VARCHAR(4096) ,"
                    + "HTTP_JSON_REQUEST_HEADERS VARCHAR(4096) ,"
                    + "HTTP_JSON_RESPONSE VARCHAR(4096) ,"
                    + "HTTP_RESPONSE_CODE INT NOT NULL ,"
                    + "HTTP_RESPONSE_FILE_TYPE VARCHAR(128) NOT NULL ,"
                    + "RUN_ORDER INT NOT NULL,"
                    + "PRIMARY KEY (ID))");
            connection.commit();
        
        }
        catch(SQLException sqle) {
            logger.info("Unable to create database.",sqle);
        }

    }
    
    public void closeDatabase() throws SQLException{
        logger.info("Closing DB:{}", dbUrl);
        connection.close();
    }
    
    private Connection getConnection() throws SQLException {
        logger.info("Connecting to DB: {}", dbUrl);
        return DriverManager.getConnection(dbUrl, "sa", "");
    }
    
    public HashMap<Long,WireMockService> readServices() {
        logger.debug("readServices");
        
        
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery("SELECT * from wire_mock_service order by run_order")) 
        {
            HashMap<Long,WireMockService>services = new HashMap<>();
            while(result.next()) {
                WireMockService record = createRecordFromResultSet(result);
                 services.put(record.getId(), record);
            }
            return services;
        }
        catch(SQLException sqle) {
            logger.info("Error reading services.",sqle);
            return null;
        }
        
    }
    
    private WireMockService createRecordFromResultSet(ResultSet result) 
    		throws SQLException {
    	WireMockService record = new WireMockService();
    	long id = result.getLong("ID");
        int port = result.getInt("PORT");
        if(this.dbPort.length() > 0) {
        	port = Integer.parseInt(dbPort);
        }
        String url = result.getString("URL");
        String httpRequestType = result.getString("HTTP_REQUEST_TYPE");
        String httpJsonRequest = result.getString("HTTP_JSON_REQUEST");
        String httpJsonRequestHeaders = result.getString("HTTP_JSON_REQUEST_HEADERS");
        String httpJsonResponse = result.getString("HTTP_JSON_RESPONSE");
        int httpResponseCode = result.getInt("HTTP_RESPONSE_CODE");
        String httpResponseFileType = result.getString("HTTP_RESPONSE_FILE_TYPE");
        long runOrder = result.getInt("RUN_ORDER");
        record.setId(id);
        record.setPort(port);
        record.setHttpJsonRequest(httpJsonRequest);
        record.setHttpJsonRequestHeaders(httpJsonRequestHeaders);
        record.setHttpJsonResponse(httpJsonResponse);
        record.setHttpRequestType(httpRequestType);
        record.setHttpResponseCode(httpResponseCode);
        record.setUrl(url);
        record.setHttpResponseFileType(httpResponseFileType);
        record.setRunOrder(runOrder);
        return record;
    	
    }
    
    public WireMockService readServiceAtIndex(long index) {
        logger.debug("readBaseRecordAtIndex");
        ResultSet result = null;
        try (PreparedStatement statement = 
                connection.prepareStatement("SELECT * FROM wire_mock_service WHERE ID = ?")) 
        {
            statement.setLong(1, index);
            result = statement.executeQuery();
            WireMockService record = new WireMockService();
            while(result.next()) {
            	record = createRecordFromResultSet(result);
            }
            return record;
        }
        catch(SQLException sqle) {
            logger.warn("Unable to read WireMockService",sqle);
            return null;
        }
        finally {
           if(null != result) {
               try {
                   result.close();
               }
               catch(SQLException sqle1) {
                   logger.warn("Unable to close result set",sqle1);
               }
           }
        }
    }
    
    /**
     * Update the database from the map of paramaters
     * The first value is the key from UPDATE_PARAMETERS
     * The second is the value
     * @param tupples
     */
    public boolean updateDatabaseRecord(HashMap<String, String>tupples) {
        try (PreparedStatement statement = 
                connection.prepareStatement("UPDATE wire_mock_service SET PORT = ?,"+
                "URL = ?,"+
                "HTTP_REQUEST_TYPE = ?,"+
                "HTTP_JSON_REQUEST = ?,"+ //4
                "HTTP_JSON_REQUEST_HEADERS = ?,"+ //5
                "HTTP_JSON_RESPONSE = ?,"+ //6
                "HTTP_RESPONSE_CODE = ?,"+ //7
                "HTTP_RESPONSE_FILE_TYPE = ?,"+ //8
                "RUN_ORDER = ? " + //9
                "WHERE ID = ?")){
            statement.setInt(1, Integer.parseInt(tupples.get(DB_PARAMETERS[1])));
            statement.setString(2, tupples.get(DB_PARAMETERS[2]));
            statement.setString(3, tupples.get(DB_PARAMETERS[3]));
            statement.setString(4, tupples.get(DB_PARAMETERS[4]));
            statement.setString(5, tupples.get(DB_PARAMETERS[5]));
            statement.setString(6, tupples.get(DB_PARAMETERS[6]));
            statement.setInt(7, Integer.parseInt(tupples.get(DB_PARAMETERS[7])));
            statement.setString(8, tupples.get(DB_PARAMETERS[8]));
            statement.setInt(9, Integer.parseInt(tupples.get(DB_PARAMETERS[9])));
            statement.setInt(10, Integer.parseInt(tupples.get(DB_PARAMETERS[0])));
            
            int noRecords = statement.executeUpdate();
            logger.info("Updated " + noRecords + " records");
            connection.commit();
            return (noRecords > 0);
        }
        catch(SQLException sqle) {
            logger.warn("Unable to execute update with values {} ",tupples, sqle);
            return false;
        }
    }
    
    public boolean deleteDatabaseRecord(long id) {
        try (PreparedStatement statement = 
                connection.prepareStatement("DELETE FROM wire_mock_service "+
                        "WHERE ID = ?")){
            statement.setLong(1, id);
            int noRecords = statement.executeUpdate();
            logger.info("Deleted " + noRecords + " records");
            connection.commit();
            return (noRecords > 0);
        }
        catch(SQLException sqle) {
            logger.warn("Unable to execute delete with id " + id);
            return false;
        }
    }
    
    /**
     * Create a new record - must have index larger than current max (unless you have
     * deleted a record and left holes !)
     * @param tupples
     * @return
     */
    public boolean createDatabaseRecord(HashMap<String, String>tupples) {
        try (PreparedStatement statement = 
               connection.prepareStatement("INSERT INTO wire_mock_service "+
                    "(ID,PORT,URL,HTTP_REQUEST_TYPE,HTTP_JSON_REQUEST,"+
                    "HTTP_JSON_REQUEST_HEADERS,HTTP_JSON_RESPONSE,HTTP_RESPONSE_CODE,HTTP_RESPONSE_FILE_TYPE,RUN_ORDER)"+
                    " VALUES (?,?,?,?,?,?,?,?,?,?)")) 
        {
            statement.setInt(1, Integer.parseInt(tupples.get(DB_PARAMETERS[0])));
            statement.setInt(2, Integer.parseInt(tupples.get(DB_PARAMETERS[1])));
            statement.setString(3, tupples.get(DB_PARAMETERS[2]));
            statement.setString(4, tupples.get(DB_PARAMETERS[3]));
            statement.setString(5, tupples.get(DB_PARAMETERS[4]));
            statement.setString(6, tupples.get(DB_PARAMETERS[5]));
            statement.setString(7, tupples.get(DB_PARAMETERS[6]));
            statement.setInt(8, Integer.parseInt(tupples.get(DB_PARAMETERS[7])));
            statement.setString(9, tupples.get(DB_PARAMETERS[8]));
            statement.setInt(10, Integer.parseInt(tupples.get(DB_PARAMETERS[9])));
            int noRecords = statement.executeUpdate();
            logger.info("Updated " + noRecords + " records");
            connection.commit();
            return (noRecords > 0);
        }
        catch(SQLException sqle) {
            logger.warn("Unable to execute create with values " + tupples);
            return false;
        }
    }
    
    /**
     * Get the maximum record id
     * @return
     */
    public int getMaxRecordId() {
        logger.debug("getMaxRecordId");
        
        int maxId = 0;
        try (Statement statement = connection.createStatement();
                ResultSet result = statement.executeQuery("SELECT MAX(ID) FROM wire_mock_service")){
            while(result.next()) {
                maxId = result.getInt(1);
            }
        }
        catch(SQLException sqle) {
            logger.warn("Unable to getMaxRecordId");
        }
        return maxId;
    }

}
