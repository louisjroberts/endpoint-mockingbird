package org.lorob.testexamples.wiremock.admin.repo;

import java.util.List;

import org.lorob.testexamples.wiremock.admin.persistence.model.WireMockService;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WireMockRepository extends JpaRepository<WireMockService, Long> {
	
	List<WireMockService> findByPort(int port);
    
    WireMockService findById(long id);
}