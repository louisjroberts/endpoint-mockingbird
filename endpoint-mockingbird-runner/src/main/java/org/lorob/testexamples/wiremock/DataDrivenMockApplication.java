package org.lorob.testexamples.wiremock;

import java.sql.SQLException;
import java.util.HashMap;

import org.lorob.testexamples.wiremock.admin.persistence.model.WireMockService;
import org.lorob.testexamples.wiremock.db.WireMockDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Application to create a WireMock instance which reads
 * from a H2 database for data to create the endpoints
 * and return behaviour of WireMock without hard coding.
 * 
 * This class can also be used as part of a set of tests. The class
 * can be created to setup the wiremocks from a database. then after the tests are run
 * call closeServices
 * 
 * DataDrivenMockApplication mockedService=new DataDrivenMockApplication(connectionString);
 * mockedService.runMocks();
 * mockedService.closeServices()
 * 
 * @author lorob
 *
 */
public class DataDrivenMockApplication implements CustomWireMockCallBack {
    
    private static Logger logger = LoggerFactory.getLogger(DataDrivenMockApplication.class);
    
    private static String sUsage = "Incorrect usage. DataDrivenMock -db [jdbc-url | use-default-db] -host host-name -driver driver-class" +
    "\ni.e. DataDrivenMockApplication -db jdbc:h2:file:./db/mockservice/mockservices -host localhost\n or DataDrivenMockApplication -db use-default-db -host localhost -driver org.h2.Driver [-port optional port to use]";
    public static String sDefaultURL = "jdbc:h2:file:./db/mockservice/mockservices";
    public static String sUseDefault = "use-default-db";
    public static String sLocalhost = "localhost";
    private WireMockDB hSQLWireMockDB;
    
    private HashMap<Long,WireMockService> wireMockServiceRecords;
    
    private CustomWireMock customWireMock;
    
    private String host = sLocalhost;
    
    /**
     * * Create the Data Driven application. 
     * @param databaseUrl
     * @param host
     * @param driver
     * @throws Exception
     */
    public DataDrivenMockApplication(String databaseUrl, String host, String driver, String port) throws Exception {
        this.host = host;
        hSQLWireMockDB = new WireMockDB(databaseUrl, driver, port);
        hSQLWireMockDB.initDatabase();
        wireMockServiceRecords = hSQLWireMockDB.readServices();
        closeDB();
    }
    
    /**
     * Close the database connection
     * This is to be used if this class is part of a test which
     * controls the instantiation and wishes to close down WireMock afterwards
     */
    public void closeDB() {
        if (null != hSQLWireMockDB) {
            try {
                hSQLWireMockDB.closeDatabase();
                hSQLWireMockDB = null;
            }
            catch(SQLException e) {
                logger.error("Failed to Close Database. Error: {}",e);
            }
        }
    }
    
    public void closeServices() {
        closeDB();
        if(null != customWireMock) {
        	customWireMock.teardown();
        }
    }
    
    public void runMocks() {
        // NOTE - need to create custom wire mocks per port
        WireMockService firstRecord = wireMockServiceRecords.get(1l);
        customWireMock = new CustomWireMock (firstRecord.getPort(),host);
        customWireMock.setCallback(this);
        // Need to order the records
        wireMockServiceRecords.forEach((index,record)
                -> {
                    logger.info("Processing index:{}, record{}",index, record);
                    customWireMock.processMockService(record);
                });
    }
    
    /**
     * args - Pass in the url of the hsql db or -use-default-db to use the default database
     * @param args
     */
    public static void main(String[] args) {
        
        HashMap<String,String> processedArguments = processArguments(args);
        if(!("true".equals(processedArguments.get("status")))) {
            logger.error(sUsage);
            return;
        }
        String connectionString = processedArguments.get("db");
        String host = processedArguments.get("host");
        String driver = processedArguments.get("driver");
        String port = processedArguments.get("port");
        try {
            DataDrivenMockApplication mockedService=new DataDrivenMockApplication(connectionString, host, driver, port);
            mockedService.runMocks();
        }
        catch(Exception e) {
            logger.error("Error in DataDrivenMock.",e);
        }
        
    }
    
    public static HashMap<String,String> processArguments(String[] args) {
        HashMap<String,String> arguments = new HashMap<String, String>();
        arguments.put("status", "false");
        if(args.length < 4) {
            return arguments;
        }
        if ("-db".equals(args[0]) && sUseDefault.equals(args[1])) {
            arguments.put("db", sDefaultURL);
        }
        else {
            arguments.put("db",args[1]);
        }
        if("-host".equals(args[2])) {
            arguments.put("host",args[3]);
            arguments.put("status","true");
        }
        if(args.length > 5 && "-driver".equals(args[4])) {
        	arguments.put("driver", args[5]);
        }
        else {
        	arguments.put("driver", "org.h2.Driver");
        }
        if(args.length > 7 && "-port".equals(args[6])) {
        	arguments.put("port", args[7]);
        }
        else {
        	arguments.put("port", "");
        }
        	
        return arguments;
    }

    @Override
    public void callback() {
        closeServices();
    }

}
