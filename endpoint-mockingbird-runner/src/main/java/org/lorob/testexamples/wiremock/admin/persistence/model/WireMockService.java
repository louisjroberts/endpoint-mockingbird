package org.lorob.testexamples.wiremock.admin.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;

@Entity
public class WireMockService {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Min(0)
    @Column(nullable = false)
    private int port;
    
    @Column(nullable = false)
    private String url;
    
    @Column
    private String httpRequestType;
    
    @Column
    private String httpJsonRequest;
    
    @Column
    private String httpJsonRequestHeaders;
    
    @Column
    private String httpJsonResponse;
    
    @Column
    private int httpResponseCode;
    
    @Column
    private String httpResponseFileType;
    
    @Column
    private Long runOrder;

    public long getId() {
        return id;
    }
   
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHttpRequestType() {
        return httpRequestType;
    }

    public void setHttpRequestType(String httpRequestType) {
        this.httpRequestType = httpRequestType;
    }

    public String getHttpJsonRequest() {
        return httpJsonRequest;
    }

    public void setHttpJsonRequest(String httpJsonRequest) {
        this.httpJsonRequest = httpJsonRequest;
    }

    public String getHttpJsonRequestHeaders() {
        return httpJsonRequestHeaders;
    }

    public void setHttpJsonRequestHeaders(String httpJsonRequestHeaders) {
        this.httpJsonRequestHeaders = httpJsonRequestHeaders;
    }

    public String getHttpJsonResponse() {
        return httpJsonResponse;
    }

    public void setHttpJsonResponse(String httpJsonResponse) {
        this.httpJsonResponse = httpJsonResponse;
    }

    public int getHttpResponseCode() {
        return httpResponseCode;
    }

    public void setHttpResponseCode(int httpResponseCode) {
        this.httpResponseCode = httpResponseCode;
    }

	public long getRunOrder() {
		return runOrder;
	}

	public void setRunOrder(Long runOrder) {
		this.runOrder = runOrder;
	}

	public String getHttpResponseFileType() {
		return httpResponseFileType;
	}

	public void setHttpResponseFileType(String httpResponseFileType) {
		this.httpResponseFileType = httpResponseFileType;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "WireMockService [id=" + id + ", port=" + port + ", url=" + url + ", httpRequestType=" + httpRequestType
				+ ", httpJsonRequest=" + httpJsonRequest + ", httpJsonRequestHeaders=" + httpJsonRequestHeaders
				+ ", httpJsonResponse=" + httpJsonResponse + ", httpResponseCode=" + httpResponseCode
				+ ", httpResponseFileType=" + httpResponseFileType + ", runOrder=" + runOrder + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((httpJsonRequest == null) ? 0 : httpJsonRequest.hashCode());
		result = prime * result + ((httpJsonRequestHeaders == null) ? 0 : httpJsonRequestHeaders.hashCode());
		result = prime * result + ((httpJsonResponse == null) ? 0 : httpJsonResponse.hashCode());
		result = prime * result + ((httpRequestType == null) ? 0 : httpRequestType.hashCode());
		result = prime * result + httpResponseCode;
		result = prime * result + ((httpResponseFileType == null) ? 0 : httpResponseFileType.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + port;
		result = prime * result + ((runOrder == null) ? 0 : runOrder.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WireMockService other = (WireMockService) obj;
		if (httpJsonRequest == null) {
			if (other.httpJsonRequest != null)
				return false;
		} else if (!httpJsonRequest.equals(other.httpJsonRequest))
			return false;
		if (httpJsonRequestHeaders == null) {
			if (other.httpJsonRequestHeaders != null)
				return false;
		} else if (!httpJsonRequestHeaders.equals(other.httpJsonRequestHeaders))
			return false;
		if (httpJsonResponse == null) {
			if (other.httpJsonResponse != null)
				return false;
		} else if (!httpJsonResponse.equals(other.httpJsonResponse))
			return false;
		if (httpRequestType == null) {
			if (other.httpRequestType != null)
				return false;
		} else if (!httpRequestType.equals(other.httpRequestType))
			return false;
		if (httpResponseCode != other.httpResponseCode)
			return false;
		if (httpResponseFileType == null) {
			if (other.httpResponseFileType != null)
				return false;
		} else if (!httpResponseFileType.equals(other.httpResponseFileType))
			return false;
		if (id != other.id)
			return false;
		if (port != other.port)
			return false;
		if (runOrder == null) {
			if (other.runOrder != null)
				return false;
		} else if (!runOrder.equals(other.runOrder))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}
	
    
}
