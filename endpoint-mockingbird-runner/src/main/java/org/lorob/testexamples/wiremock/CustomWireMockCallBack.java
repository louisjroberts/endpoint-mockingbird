package org.lorob.testexamples.wiremock;

/**
 * Callback, called by CustomWireMock when WireMock has been
 * told to close. Allows any tidy up
 * Main purpose is to shut down the database
 * @author lorob
 *
 */
public interface CustomWireMockCallBack {
    public void callback();
}
