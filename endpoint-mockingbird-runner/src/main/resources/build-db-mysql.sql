CREATE USER 'appuser'@'localhost' IDENTIFIED BY 'password1';
GRANT ALL PRIVILEGES ON * . * TO 'appuser'@'localhost';
-- the following are alternatives. for some reason mysql fails to connect to localhost and 
-- uses an internal IP so the following may work. The IP address is given in the error message
--CREATE USER 'appuser'@'10.4.0.1' IDENTIFIED BY 'password1';
--GRANT ALL PRIVILEGES ON *.* TO 'appuser'@'10.4.0.1';
-- Note MySql tables musrt be in lower case

CREATE DATABASE mockservicedb;
CREATE TABLE wire_mock_service (ID INT NOT NULL auto_increment PRIMARY KEY, 
                    PORT INT NOT NULL,
                    URL VARCHAR(2048) NOT NULL,
                    HTTP_REQUEST_TYPE VARCHAR(128) NOT NULL,
                    HTTP_JSON_REQUEST VARCHAR(4096) ,
                    HTTP_JSON_REQUEST_HEADERS VARCHAR(4096) ,
                    HTTP_JSON_RESPONSE VARCHAR(4096) ,
                    HTTP_RESPONSE_CODE INT NOT NULL ,
                    HTTP_RESPONSE_FILE_TYPE VARCHAR(128) NOT NULL ,
                    RUN_ORDER INT NOT NULL);

INSERT INTO wire_mock_service(PORT,URL,HTTP_REQUEST_TYPE,HTTP_JSON_REQUEST,HTTP_JSON_REQUEST_HEADERS,HTTP_JSON_RESPONSE,HTTP_RESPONSE_CODE,HTTP_RESPONSE_FILE_TYPE,RUN_ORDER) VALUES(9999,'/testservice','GET','','{"Content-Type":"application/json; charset=UTF-8"}','{"status":"OK"}',200,'application/json',1);
INSERT INTO wire_mock_service(PORT,URL,HTTP_REQUEST_TYPE,HTTP_JSON_REQUEST,HTTP_JSON_REQUEST_HEADERS,HTTP_JSON_RESPONSE,HTTP_RESPONSE_CODE,HTTP_RESPONSE_FILE_TYPE,RUN_ORDER) VALUES(9999,'/testservice?value=1','GET','','{"Content-Type":"application/json; charset=UTF-8"}','{"status":"FAIL"}',403,'application/json',2);
INSERT INTO wire_mock_service(PORT,URL,HTTP_REQUEST_TYPE,HTTP_JSON_REQUEST,HTTP_JSON_REQUEST_HEADERS,HTTP_JSON_RESPONSE,HTTP_RESPONSE_CODE,HTTP_RESPONSE_FILE_TYPE,RUN_ORDER) VALUES(9999,'/testservice?value=2','GET','','{"Content-Type":"application/json; charset=UTF-8"}','{"status":"FAIL"}',500,'application/json',3);
INSERT INTO wire_mock_service(PORT,URL,HTTP_REQUEST_TYPE,HTTP_JSON_REQUEST,HTTP_JSON_REQUEST_HEADERS,HTTP_JSON_RESPONSE,HTTP_RESPONSE_CODE,HTTP_RESPONSE_FILE_TYPE,RUN_ORDER) VALUES(9999,'/testservicepost','POST','{"value":"1"}','{"Content-Type":"application/json; charset=UTF-8"}','{"status":"OK"}',200,'application/json',4);
INSERT INTO wire_mock_service(PORT,URL,HTTP_REQUEST_TYPE,HTTP_JSON_REQUEST,HTTP_JSON_REQUEST_HEADERS,HTTP_JSON_RESPONSE,HTTP_RESPONSE_CODE,HTTP_RESPONSE_FILE_TYPE,RUN_ORDER) VALUES(9999,'/testservicepost','POST','{"value":"2"}','{"Content-Type":"application/json; charset=UTF-8"}','{"status":"FAIL"}',403,'application/json',5);
INSERT INTO wire_mock_service(PORT,URL,HTTP_REQUEST_TYPE,HTTP_JSON_REQUEST,HTTP_JSON_REQUEST_HEADERS,HTTP_JSON_RESPONSE,HTTP_RESPONSE_CODE,HTTP_RESPONSE_FILE_TYPE,RUN_ORDER) VALUES(9999,'/testservice?value=3','GET','','{"Content-Type":"application/json; charset=UTF-8"}','{"status":"OK"}',200,'application/json',6);
INSERT INTO wire_mock_service(PORT,URL,HTTP_REQUEST_TYPE,HTTP_JSON_REQUEST,HTTP_JSON_REQUEST_HEADERS,HTTP_JSON_RESPONSE,HTTP_RESPONSE_CODE,HTTP_RESPONSE_FILE_TYPE,RUN_ORDER) VALUES(9999,'/testservice?value=4&value2=4','GET','','{"Content-Type":"application/json; charset=UTF-8"}','{"status":"OK"}',200,'application/json',7);
INSERT INTO wire_mock_service(PORT,URL,HTTP_REQUEST_TYPE,HTTP_JSON_REQUEST,HTTP_JSON_REQUEST_HEADERS,HTTP_JSON_RESPONSE,HTTP_RESPONSE_CODE,HTTP_RESPONSE_FILE_TYPE,RUN_ORDER) VALUES(9999,'/testservice?value=4','GET','','{"Content-Type":"application/json; charset=UTF-8"}','{"status":"OK"}',200,'application/json',8);
INSERT INTO wire_mock_service(PORT,URL,HTTP_REQUEST_TYPE,HTTP_JSON_REQUEST,HTTP_JSON_REQUEST_HEADERS,HTTP_JSON_RESPONSE,HTTP_RESPONSE_CODE,HTTP_RESPONSE_FILE_TYPE,RUN_ORDER) VALUES(9999,'/testservice?value=5','GET','','{"Content-Type":"application/json; charset=UTF-8"}','{"status":"OK"}',200,'application/json',9);
INSERT INTO wire_mock_service(PORT,URL,HTTP_REQUEST_TYPE,HTTP_JSON_REQUEST,HTTP_JSON_REQUEST_HEADERS,HTTP_JSON_RESPONSE,HTTP_RESPONSE_CODE,HTTP_RESPONSE_FILE_TYPE,RUN_ORDER) VALUES(9999,'/testservicepatch','PATCH','','{"Content-Type":"application/json; charset=UTF-8"}','{"status":"OK"}',200,'application/json',10);
INSERT INTO wire_mock_service(PORT,URL,HTTP_REQUEST_TYPE,HTTP_JSON_REQUEST,HTTP_JSON_REQUEST_HEADERS,HTTP_JSON_RESPONSE,HTTP_RESPONSE_CODE,HTTP_RESPONSE_FILE_TYPE,RUN_ORDER) VALUES(9999,'/testservicedelete','DELETE','','{"Content-Type":"application/json; charset=UTF-8"}','{"status":"OK"}',200,'application/json',11);
INSERT INTO wire_mock_service(PORT,URL,HTTP_REQUEST_TYPE,HTTP_JSON_REQUEST,HTTP_JSON_REQUEST_HEADERS,HTTP_JSON_RESPONSE,HTTP_RESPONSE_CODE,HTTP_RESPONSE_FILE_TYPE,RUN_ORDER) VALUES(9999,'/testservicehtml','GET','','{"Content-Type":"text/html; charset=UTF-8"}','<html><head><title>Test Page</title></head><body>Hello</body></html>',200,'text/html',12);